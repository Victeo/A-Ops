import configparser
from common.util import SingletonMeta
from inside_backend.settings import AOPS_CONF


def parse_config(conf_file: str, section: str, key: str):
    config = configparser.ConfigParser()
    try:
        config.read(conf_file)
        value = config[section][key]
    except (configparser.Error, KeyError):
        return None
    return value


def parse_bool_config(conf_file: str, section: str, key: str):
    config = configparser.ConfigParser()
    try:
        config.read(conf_file)
        value = config.getboolean(section, key)
    except (configparser.Error, ValueError):
        return False
    return value


class AopsConfigParser(metaclass=SingletonMeta):
    def __init__(self):
        self.prometheus_ip = parse_config(AOPS_CONF, "Prometheus", "ip")
        self.prometheus_port = parse_config(AOPS_CONF, "Prometheus", "port")
        self.arangodb_ip = parse_config(AOPS_CONF, "ArangoDB", "ip")
        self.arangodb_port = parse_config(AOPS_CONF, "ArangoDB", "port")
        self.arangodb_db = {
            "spider": parse_config(AOPS_CONF, "ArangoDB", "topo_db"),
            "event": parse_config(AOPS_CONF, "ArangoDB", "event_db")
        }
        self.kafka_ip = parse_config(AOPS_CONF, "Kafka", "ip")
        self.kafka_port = parse_config(AOPS_CONF, "Kafka", "port")
        self.kafka_topics = {
            'abnormal': parse_config(AOPS_CONF, "Kafka", "abnormal_topic"),
            'root_cause': parse_config(AOPS_CONF, "Kafka", "root_cause_topic"),
            'metadata': parse_config(AOPS_CONF, "Kafka", "metadata_topic"),
            'event': parse_config(AOPS_CONF, "Kafka", "event_topic")
        }
        self.fileserver_ip = parse_config(AOPS_CONF, "fileserver", "ip")
        self.fileserver_port = parse_config(AOPS_CONF, "fileserver", "port")
        self.fault_interval = int(parse_config(AOPS_CONF, "Default", "fault_interval"))
