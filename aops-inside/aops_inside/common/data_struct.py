from enum import Enum, auto, unique


class AutoName(Enum):
    def _generate_next_value_(name: str, start: int, count: int, last_values: list):
        return name.lower()


@unique
class RespondStatus(AutoName):
    SUCCESS = auto()
    FAILURE = auto()

    def __str__(self):
        return self.value


class RespondResult:
    def __init__(self):
        self.status = str(RespondStatus.FAILURE)
        self.data = None


class ArangoRelation(AutoName):
    BELONGS_TO = auto()
    CONNECT = auto()
    RUNS_ON = auto()
    IS_PEER = auto()
    STORE_IN = auto()

    def __str__(self):
        return self.value


class HorizontalTopoType(AutoName):
    APP = auto()
    HOST = auto()
    PROC = auto()

    def __str__(self):
        return self.value
