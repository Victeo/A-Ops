import time
from django.core.paginator import Paginator

# 便于开发进行本地配置调试
from common.config import AopsConfigParser
from common.data_struct import ArangoRelation
from data_collection.arangodb import ArangodbCollector
from data_collection.prometheus import PrometheusCollector

CONF = AopsConfigParser()


def connect_prometheus():
    if CONF.prometheus_port is None or CONF.prometheus_ip is None:
        return None

    return PrometheusCollector(server_ip=CONF.prometheus_ip, port=CONF.prometheus_port)


def connect_arangodb(db_name: str):
    if CONF.arangodb_ip is None or CONF.arangodb_port is None or db_name is None:
        return None

    return ArangodbCollector(server_ip=CONF.arangodb_ip, port=CONF.arangodb_port)


def get_entity_ip(machine_id: str = ''):
    prometheus = connect_prometheus()
    ret = prometheus.get_series("cpu_user_total_second",
                                {"machine_id": machine_id, "cpu": "0"})[0]
    return str(ret['instance']).split(':')[0]


def time_cost(func):
    """
    函数耗时装饰器
    """

    def call_func(*args, **kwargs):
        start = time.time()
        res = func(*args, **kwargs)
        print(f"{func.__name__} cost time: {time.time() - start}s.")
        return res

    return call_func


def get_entity_event_mapping(event_info: list, event_dict: dict, entity_key: str):
    if event_info:
        for item in event_info:
            if event_dict.get(entity_key):
                event_dict[entity_key].add(item['event_id'])
            else:
                event_dict[entity_key] = {item['event_id']}
    return event_dict


def get_abnormal_mapping(arango: ArangodbCollector, entity_id: str = None):
    entity_event = {}

    abnormal_type = ('proc', 'thread', 'tcp_link', 'endpoint', 'runtime', 'sli', 'qdisc',
                     'block', 'app')
    host_list = []
    abnormal_dict = {}
    # 未传入实体id时获取全部主机的异常信息
    if entity_id:
        host_list.append(entity_id)
    else:
        for item in arango.get_elements_by_filter(filters={'type': 'host'}):
            host_list.append(item['_id'])
    for host_id in host_list:
        abnormals = arango.get_host_abnormal_entities(host_id)
        host_key = host_id.split('/')[1]
        if abnormals:
            for key in abnormals.keys():
                for item in abnormals[key]:
                    entity_event = get_entity_event_mapping(item['abn_metrics'], entity_event,
                                                            host_key)
                if abnormal_dict.get(key):
                    abnormal_dict[key].extend(abnormals[key])
                else:
                    abnormal_dict[key] = abnormals[key]
    # 异常传播
    for key in abnormal_dict.keys():
        for abnormal_entity in abnormal_dict[key]:
            entity_key = abnormal_entity['entity']['_key']
            event_info = arango.get_abnormal_metrics_by_id(abnormal_entity['entity']['_id'])
            entity_event = get_entity_event_mapping(event_info, entity_event, entity_key)
            if key not in abnormal_type:
                continue
            ret = arango.get_out_edges(start=abnormal_entity['entity']['_id'],
                                       relation_type=str(ArangoRelation.BELONGS_TO))
            for entity in ret:
                if entity['type'] == 'proc':
                    app = arango.get_out_edges(start=entity['_id'], entity_type='app',
                                               relation_type=str(ArangoRelation.BELONGS_TO))
                    if app:
                        entity_event = get_entity_event_mapping(event_info, entity_event,
                                                                app[0]['_key'])
                entity_event = get_entity_event_mapping(event_info, entity_event, entity['_key'])

    return entity_event


def combine_description_name(entity: dict):
    if entity['type'] == 'host':
        return f"{entity['hostname']}"
    if entity['type'] == 'cpu':
        return f"cpu{entity['cpu']}"
    if entity['type'] == 'proc':
        return f"{entity['tgid']}_{entity['comm']}"
    if entity['type'] == 'block':
        return f"{entity['major']}_{entity['first_minor']}"
    if entity['type'] == 'container':
        return f"{entity['name']}_{entity['container_id']}"
    if entity['type'] == 'app':
        return entity['app_name']
    if entity['type'] == 'sli':
        return f"{entity['tgid']}_{entity['ins_id']}_{entity['app']}_{entity['method']}"
    if entity['type'] == 'disk':
        return entity['disk_name']
    if entity['type'] == 'nic':
        return entity['dev_name']
    if entity['type'] == 'tcp_link':
        return f"{entity['client_ip']}:{entity['client_port']}_" \
               f"{entity['server_ip']}:{entity['server_port']}"
    if entity['type'] == 'fs':
        return entity['MountOn']
    if entity['type'] == 'net':
        return entity['origin']
    if entity['type'] == 'thread':
        return f"{entity['pid']}_{entity['tgid']}_{entity['comm']}"
    if entity['type'] == 'qdisc':
        return f"{entity['dev_name']}_{entity['handle']}_{entity['ifindex']}"
    if entity['type'] == 'endpoint':
        return f"{entity['tgid']}_{entity['s_addr']}"
    if entity['type'] == 'mem':
        return entity['infos']


def get_page_info(data: list, cur_page: int, page_size: int):
    # 每页十条数据
    paginator = Paginator(data, page_size)
    max_pages = paginator.num_pages
    # 当前页码大于最大页码时返回最后一页，小于最小页码时返回第一页
    if cur_page > max_pages:
        cur_page = max_pages
    elif cur_page < 1:
        cur_page = 1

    return paginator.count, list(paginator.page(cur_page)), cur_page
