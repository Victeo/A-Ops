def init():
    global _CUSTOM_CONFIG
    _CUSTOM_CONFIG = {}


def set_value(machine_id: str, entity_type: str, metric: str, value: bool):
    if not _CUSTOM_CONFIG.get(machine_id):
        _CUSTOM_CONFIG[machine_id] = {}
    if not _CUSTOM_CONFIG[machine_id].get(entity_type):
        _CUSTOM_CONFIG[machine_id][entity_type] = {}
    _CUSTOM_CONFIG[machine_id][entity_type][metric] = bool(value)


def get_value(machine_id: str, entity_type: str, metric: str):
    if _CUSTOM_CONFIG.get(machine_id) and \
            _CUSTOM_CONFIG.get(machine_id).get(entity_type):
        return _CUSTOM_CONFIG.get(machine_id).get(entity_type).get(metric, None)
    else:
        return None
