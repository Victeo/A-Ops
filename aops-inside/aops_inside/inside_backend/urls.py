"""inside_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from views.flame_graph_view import get_flame_graph_by_day
from views.main import index, get_overview
from views.host_info import get_host_list, get_host_topo, get_entity_params_by_type, \
    get_entities_value_by_label
from views.host_info import get_detail_by_id, get_children_topo
from views.horizontal_topo_info import get_horizontal_topo
from views.fault_diagnosis import get_abnormal_list_by_time_range, fault_topos
from views.fault_diagnosis import get_entity_related_abnormal_list
from views.modify_metrics_config import save_custom_config, save_global_config, \
    display_metrics_total_type_list
from views.modify_metrics_config import display_custom_config, display_global_config

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index),
    path('host_list', get_host_list),
    path('host_info', get_host_topo),
    path('entity_detail', get_detail_by_id),
    path('child_topo', get_children_topo),
    path('overview', get_overview),
    path('horizontal_topo', get_horizontal_topo),
    path('fault_list', get_abnormal_list_by_time_range),
    path('fault_topo', fault_topos),
    path('releated_fault', get_entity_related_abnormal_list),
    path('get_all_entity_types', display_metrics_total_type_list),
    path('set_custom_config', save_custom_config),
    path('set_global_config', save_global_config),
    path('get_custom_config', display_custom_config),
    path('get_global_config', display_global_config),
    path('get_flame_list', get_flame_graph_by_day),
    path('get_labels_by_type', get_entity_params_by_type),
    path('get_values_by_label', get_entities_value_by_label),
]
