from django.apps import AppConfig
from data_collection.observe_entity import start_metadata_thread

class InsideBackendConfig(AppConfig):
    name = 'inside_backend'
    def ready(self):
        start_metadata_thread()

