import time
import copy
from pyArango.connection import Connection
from pyArango.database import Database
from pyArango.collection import Collection
from pyArango.document import Document
from pyArango.query import Cursor
from pyArango.theExceptions import *
from common.util import SingletonMeta
from common.config import AopsConfigParser
from data_collection.kafka_adpter import KafkaCollector
from data_collection.observe_entity import get_observe_entity_types

DB_TOPO = 'spider'
DB_EVENT = 'event'
COLLECTION_PREFIX = 'ObserveEntities'
COLLECTION_TIMESTAMP = 'Timestamps'
COLLECTION_ABNORMAL = 'abnormal_event'
COLLECTION_ROOT_CAUSE = 'root_cause_event'
BATCHSIZE = 100
DEFAULT_INTERVAL = 300 # unit:second
ARANGODB_EXPIRE_TIME = (3600 * 24 * 30) # unit:second
METRIC_PREFIX = 'gala_gopher'

RELATION = ['runs_on', 'belongs_to', 'connect', 'is_peer']

def _collection_name(ts):
    return f"{COLLECTION_PREFIX}_{ts}"

def _document_id_by_coll(coll, key):
    return f"{coll}/{key}"

def _document_id_by_ts(ts, key):
    return f"{_collection_name(ts)}/{key}"

def _parse_document_id(eid):
    """
    ObserveEntities_1661853510/7f03eac45893479092ba2faad3e55203_thread_624343
    """
    sp = eid.split('/')
    if len(sp) < 2:
        print(f"Invalided document id format:{eid}")
        return None
    coll_name = sp[0]
    document_key = sp[1]
    sp = coll_name.split('_')
    timestamp = -1 if (len(sp) < 2) or (not sp[1].isdigit()) else sp[1]
    machine_id = document_key.split('_', maxsplit=1)[0]
    entity_type = _fetch_entity_type(document_key[:document_key.find(':')])
    return document_key, int(timestamp), machine_id, entity_type

def _fetch_entity_type(s):
    """
    7f03eac45893479092ba2faad3e55203_thread_624343
    or 
    gala_gopher_sli_rtt_nsec
    output: thread, sli
    """
    types = get_observe_entity_types()
    for v in types:
        if v in s:
            return v
    return None

def _strip_metric_prefix(metric_name, metric_type):
    return metric_name.replace(f'{METRIC_PREFIX}_{metric_type}_', '')

def _fetch_metric_name(metric):
    """
    metric: gala_gopher_sli_rtt_nsec
    输出: rtt_nsec
    """
    etype = _fetch_entity_type(metric)
    return _strip_metric_prefix(metric, etype)

def _filter_options_format(filter_dic):
    """
    将字典转换成arangodb filter格式
    输入：
        {'type':'proc', 'comm':'redis'}
    输出:
        "v.type == 'proc' and v.comm == 'redis'"
    """
    if not filter_dic: return '{}'
    filter_ops = ['v.{} == \'{}\''.format(k, v) for k, v in filter_dic.items()]
    filter_str = ' and '.join(filter_ops)
    return filter_str

def kafka_to_arangodb(message, topic):
    ardb = ArangodbCollector()
    config = AopsConfigParser()
    try:
        if topic == config.kafka_topics['abnormal'] or topic == config.kafka_topics['event']:
            desc = message['Resource'].get('description', '')
            obj = {
                '_key': message['Attributes']['event_id'],
                'Timestamp': message['Timestamp'] // 1000,
                'entity_id': message['Attributes']['entity_id'],
                'metrics': message['Resource']['metric'],
                'event_id': message['Attributes']['event_id'],
                'envent_type': message['Attributes']['event_type'],
                'SeverityNumber': message['SeverityNumber'],
                'SeverityText': message['SeverityText'],
                'desc': desc,
                'Body': message['Body'],
            }
            ardb.create_document(obj, DB_EVENT, COLLECTION_ABNORMAL)
        elif topic == config.kafka_topics['root_cause']:
            obj = {'_key': message['event_id']}
            obj.update(message)
            obj['Timestamp'] //= 1000
            ardb.create_document(obj, DB_EVENT, COLLECTION_ROOT_CAUSE)
    except Exception as e:
        print(f"kafka to arangodb failed.{repr(e)}")

def kafka_to_arangodb_startup():
    config = AopsConfigParser()
    topics = [config.kafka_topics['abnormal'], config.kafka_topics['root_cause'], config.kafka_topics['event']]
    t = KafkaCollector(config.kafka_ip, config.kafka_port, topics, 'myGroup', kafka_to_arangodb)
    t.start()

class ArangodbCollector(metaclass=SingletonMeta):
    def __init__(self, server_ip, port):
        self.server_ip = server_ip
        self.port = port
        arango_url = f'http://{self.server_ip}:{self.port}'
        try:
            self.conn = Connection(arango_url)
            self.db = self.conn[DB_TOPO]
            if not self.conn.hasDatabase(DB_EVENT):
                self.conn.createDatabase(DB_EVENT)
            self.db_event = self.conn[DB_EVENT]
            if not self.db_event.hasCollection(COLLECTION_ABNORMAL):
                self.db_event.createCollection(name = COLLECTION_ABNORMAL)
            if not self.db_event.hasCollection(COLLECTION_ROOT_CAUSE):
                self.db_event.createCollection(name = COLLECTION_ROOT_CAUSE)
            print(f"Connect to arangodb {arango_url}.")
        except:
            raise Exception(f"Failed to init arangodb {arango_url}.")
        try:
            self._create_ttl_index(COLLECTION_ABNORMAL, ARANGODB_EXPIRE_TIME)
            self._create_ttl_index(COLLECTION_ROOT_CAUSE, ARANGODB_EXPIRE_TIME)
        except:
            print('Create ttl index failed.')
        try:
            kafka_to_arangodb_startup()
        except:
            raise Exception("Failed to start kafka_to_arangodb thread.")

    def _create_ttl_index(self, collname, expire):
        coll = self.db_event[collname]
        idxs = coll.getIndexes()
        ttl_index = idxs.get('ttl')
        if ttl_index:
            idx = ttl_index.popitem()[1]
            idx.delete()
        coll.ensureTTLIndex(fields=['Timestamp'], expireAfter=expire)

    def create_document(self, obj, dbname, collname):
        try:
            db = self.conn[dbname]
            coll = db.collections[collname]
            doc = coll.createDocument(obj)
            doc.save(overwrite=True)
        except Exception as e:
            print(f"Create document failed: {repr(e)}")
            return False
        return True

    def _aql_query(self, dbname, query_str, bind_vars):
        try:
            res = self.conn[dbname].AQLQuery(query_str, bindVars=bind_vars, batchSize=BATCHSIZE, rawResults=True)
        except Exception as e:
            print(f"Arangodb query failed. {repr(e)}")
            return []
        return [v for v in res if v != None]

    def _get_recent_timestamp(self, timestamp: int):
        """
        获取数据库中与timestamp最相近的一个timestamp,
        返回值不为空说明数据库中存在该timestamp观测实体集合
        """
        bind_vars = {'@COLL': COLLECTION_TIMESTAMP, 'ts': timestamp}
        aql_query = '''
        FOR t IN @@COLL
          FILTER TO_NUMBER(t._key) <= @ts
          SORT t._key DESC
          LIMIT 1
          RETURN t._key
        '''
        data = self._aql_query(DB_TOPO, aql_query, bind_vars)
        return int(data[0]) if len(data) else None

    def _get_recent_collection(self, timestamp: int = None, tolerent_backtrace: int = None):
        """
        获取数据库中与timestamp最相近的观测实体集合, 不传入timestamp默认当前系统时间
        tolerent_backtrace: 表示容忍的回溯时间, 默认无限制
        """

        if not timestamp:
            timestamp = int(time.time())
        # hasCollection cost too much time
        #  coll = _collection_name(timestamp)
        #  if self.db.hasCollection(coll):
        #      return coll
        recent_ts = self._get_recent_timestamp(timestamp)
        if not recent_ts:
            print(f"Cannot found recent timestamp with timestamp {timestamp}")
            return None
        if tolerent_backtrace and timestamp - recent_ts > tolerent_backtrace:
            print(f"Cannot found recent timestamp with timestamp {timestamp},"
                  f"tolerent time is {tolerent_backtrace} seconds.")
            return None
        coll = _collection_name(recent_ts)
        return coll

    def get_horizontal_level_topo(self, timestamp: int = None, filters: dict = {}) -> list:
        """
        获取某个时间点实体间的水平拓扑
        输入：
            timestamp: <unix_time>时间戳，不提供timestamp默认当前系统时间
            filters: 过滤条件，多个过滤条件是and关系, 过滤字段参考观测实体属性,
                     filters={'type':'proc'}表示获取'proc'的水平topo
        输出：
            失败或没有数据返回空列表
            成功返回边列表，边是包含from/to键值的字典，from表示边的起点，to是边的终点
            格式：
            [
                {'_from': '<entity_key>'', '_to': 'entity_key'}
                {'_from': {entity_key}, '_to': None} --> 表示单节点topo
            ]
        """
        data = []
        entities = self.get_elements_by_filter(timestamp, filters, ['_id', '_key'])
        if not entities:
            return []
        for entity in entities:
            edge_nodes = self._get_edges(entity['_id'], 'connect', filters, 'OUTBOUND')
            if not len(edge_nodes):
                edges = [{'_from':entity['_key'], '_to':None}]
            else:
                edges = [{'_from':entity['_key'], '_to':v['_key']} for v in edge_nodes]
            data.extend(edges)
        return data

    def get_elements_by_filter(self, timestamp = None, filters: dict = {}, 
                               ret_fields: list = []) -> list:
        """
        filters = {'type':'proc', 'comm':'gala-gopher'}
        根据过滤条件筛选某个时间点的观测对象信息
        输入：
            timestamp: <unix_time>时间戳，不提供timestamp默认当前系统时间
            filters: 过滤条件，多个过滤条件是and关系, 过滤字段参考观测实体属性
            ret_fields: 定义返回的属性字段, 默认返回实体的全部属性
        输出：
            失败或没有数据返回空列表
            成功返回满足条件的观测对象列表
            格式：
            [
                {entity1}
                {entity2}
                ...
            ]
        """
        coll = self._get_recent_collection(timestamp)
        if not coll:
            return []
        bind_vars = {'@COLL': coll}

        filter_str = _filter_options_format(filters)

        ret_pattern = ["\'{}\':v.{}".format(k, k) for k in ret_fields]
        ret_str = ','.join(ret_pattern)
        ret_str = f"{{{ret_str}}}" if ret_str  else 'v'

        aql_query = '''
        FOR v IN @@COLL
            FILTER {}
            return {}
        '''.format(filter_str, ret_str)
        data = self._aql_query(DB_TOPO, aql_query, bind_vars)
        return data

    def get_element_by_ids(self, ids: list, dbname: str = None) -> list:
        """
        根据id列表获取观测对象实体信息,返回顺序与id在列表中的顺序一致
        输入：
            ids: 实体id列表
            dbname: 数据库名, 默认DB_TOPO
        输出：
            失败或没有数据返回空列表
            成功返回对象实体信息列表
            格式：
            [
                {'<entity1>'},
                {'<entity2>'},
                ...
            ]
        """
        dbname = dbname if dbname else DB_TOPO
        list_ids = ",".join("\"{}\"".format(eid) for eid in ids)
        aql_query = '''
        for id in [{}]
            return document(id)
        '''.format(list_ids)
        data = self._aql_query(dbname, aql_query, {})
        return data

    def get_element_by_id(self, eid: str, dbname: str = None) -> dict:
        """
        根据id获取观测对象实体信息
        输入：
            eid: 具体格式参考arangodb id格式
            dbname: 数据库名, 默认DB_TOPO
        输出：
            失败或没有数据返回None
            成功返回字典格式观测对象实体信息
            格式：
            {
                '_id': 'xxx',
                '_key': 'xxx',
                'type': 'xxx',
                'timestamp': 'xxx',
                'level': 'xxx',
                '<label1>': '<value>',
                '<label2>': '<value>',
                ...
                'metrics': {},
            }
        """
        data = self.get_element_by_ids([eid], dbname)
        return data[0] if len(data) else None

    def get_element_by_key(self, key: str, timestamp: int = None, 
                           tolerent_backtrace: int = None) -> dict:
        """
        根据key获取观测对象实体信息
        输入：
            key: 观测实体key
            timestamp: <unix_time>时间戳，不提供timestamp默认当前系统时间
            tolerent_backtrace: 表示容忍的回溯时间, 默认无限制
        输出：同get_element_by_id
        """
        coll_name = self._get_recent_collection(timestamp, tolerent_backtrace)
        if not coll_name:
            print(f"Cannot found element {key} at timestamp {timestamp} with "
                  f"tolerent_backtrace {tolerent_backtrace}")
            return None
        return self.get_element_by_id(_document_id_by_coll(coll_name, key))

    def _get_edges(self, start: str, relation_type: str,
                   filters: dict = {}, direction: str = 'ANY') -> list:
        if relation_type not in RELATION:
            print(f'Unkown relation type {relation_type}')
            return []
        #  types = get_observe_entity_types()
        #  if entity_type and entity_type not in types:
        #      print(f'Unkown filter entity type {entity_type}')
        #      return []

        bind_vars = {'@edge_coll': relation_type, 'start': start}
        filter_str = _filter_options_format(filters)
        aql_query = '''
        FOR v IN 1 {dir} @start @@edge_coll
            FILTER {filter}
            return distinct v
        '''.format(dir=direction, filter=filter_str)
        data = self._aql_query(DB_TOPO, aql_query, bind_vars)
        return data

    def get_in_edges(self, start: str, relation_type: str,
                     entity_type: str = None) -> list:
        """
        以某个观察对象为起点，根据边类型和节点类型获取入方向相邻边节点
        输入：
            start: 观测对象id, 作为图查询的起点
            relation_type: 边类型(参考RELATION)
            entity_type: 观测对象类型(参考ENTITY_TYPE), 默认不区分类型
        输出：
            失败或没有数据返回空
            成功返回观测对象实体列表
            格式：
            [
                entity1,
                entity2,
                ...
            ]
        示例：
                                         host2
                                          ^
                                          |connect
                                          |
                            belongs_to    |     runs_on
                         cpu ----------> host1 <-------- proc
                                          ^
                                          |
                                          | belongs_to
                                          |
                                          |
                                         disk
            host1->host2是host1的out方向, cpu->host1是host1是in方向
            以host1为起点，从belongs_to边中获取所有cpu类型的观测对象实体
            start = 'xxx/xxx_host'
            relation_type = 'belongs_to'
            entity_type = 'cpu'
            ent = get_in_edges(start, relation_type, entity_type)
        """
        filters = {} if not entity_type else {'type':entity_type}
        return self._get_edges(start, relation_type, filters, 'INBOUND')

    def get_out_edges(self, start: str, relation_type: str,
                      entity_type: str = None) -> list:
        """
        同get_in_edges, 获取out方向相邻边节点
        """
        filters = {} if not entity_type else {'type':entity_type}
        return self._get_edges(start, relation_type, filters, 'OUTBOUND')

    def get_both_edges(self, start: str, relation_type: str, 
            entity_type: str = None) -> list:
        """
        同get_in_edges, 获取in/out方向相邻边节点
        """
        filters = {} if not entity_type else {'type':entity_type}
        return self._get_edges(start, relation_type, filters, 'ANY')

    def _get_abnormal_root_cause_entries(self, start, end, 
            interval = DEFAULT_INTERVAL, get_abnormal = True, match = ''):
        coll = COLLECTION_ABNORMAL if get_abnormal else COLLECTION_ROOT_CAUSE
        bind_vars = {'@COLL': coll, 'ts1': start, 'ts2': end}
        if match:
            match = f"FILTER v.entity_id LIKE \"%{match}%\""
        aql_query = '''
        FOR v IN @@COLL
          FILTER v.Timestamp >= @ts1 and v.Timestamp <= @ts2
          {}
          SORT v.Timestamp DESC
          RETURN v
        '''.format(match)
        data = self._aql_query(DB_EVENT, aql_query, bind_vars)

        unique_data = []
        range_dict = {}
        cur_ts = start
        for ent in data:
            if get_abnormal:
                key = (ent['entity_id'], ent['metrics'])
                ent['machine_id'] = ent['entity_id'].split('_', maxsplit = 1)[0]
            else:
                key = (ent['Resource']['abnormal_kpi']['entity_id'], 
                        ent['Resource']['abnormal_kpi']['metric_id'])
            if key in range_dict and range_dict[key] - ent['Timestamp'] <= interval:
                continue
            range_dict[key] = ent['Timestamp']
            unique_data.append(ent)

        return unique_data

    def get_abnormal_entries(self, start, end, interval = DEFAULT_INTERVAL, match = ''):
        """
        获取一段时间内的异常数据
        输入：
            start: <unix_time>时间戳，开始时间点
            end: <unix_time>时间戳，结束时间点
            interval: 筛选重复异常的间隔(s)，即相同异常的间隔大于inetrval才会被记录
                    (entity_id与metrics作为异常的key)
            match: 用于过滤entity_id包含match字串的异常
        输出：
            失败或没有数据返回空
            成功返回异常列表
            格式：
            [
                {abnormal_entity1},
                {abnormal_entity2},
                ...
            ]
            
             abnormal_entity格式：
             {
               '_id':  --> document id
               '_key': --> document key 
               '_rev': --> document version
               'Timestamp': --> <unix_time>时间戳
               'entity_id': --> 异常指标对应的实体id
               'metrics': '--> 异常指标
               'event_id': --> 异常事件ID，用于获取根因topo
               'event_type': --> 事件类型，分为系统和应用异常(sys/app), 应用异常才有根因topo
               'machine_id': --> 主机machine id
               'SeverityNumber': --> 异常严重等级编号
               'SeverityText': --> 异常等级描述(INFO/9 WARN/13 ERROR/17 FATAL/21)
               'desc': --> 异常中文描述
               'Body': --> 异常说明
             }
            
        """
        return self._get_abnormal_root_cause_entries(start, end, interval, True, match)

    def _get_abnormal_entities(self, ts, match = ''):
        abns = self.get_abnormal_entries(ts - DEFAULT_INTERVAL, ts, match = match)
        abns_ents = {}
        eid_dict = {} # {entity_id : [<abn>, <abn>, ...]}

        for abn in abns:
            eid = _document_id_by_ts(ts, abn['entity_id'])
            entity = self.get_element_by_id(eid)
            if not entity:
                print(f"No entity related with {eid} in collection {_collection_name(ts)}")
                continue
            striped_metric = _strip_metric_prefix(abn['metrics'], entity['type'])
            metric_array = eid_dict.setdefault(abn['entity_id'], [])
            abn_metric = {
                'metrics': striped_metric,
                'desc': abn['desc'],
                'event_id': f"{COLLECTION_ROOT_CAUSE}/{abn['event_id']}",
            }
            if metric_array:
                metric_array.append(abn_metric)
                continue
            metric_array.append(abn_metric)
            tmp = abns_ents.setdefault(entity['type'], [])
            tmp.append({'entity':entity, 'abn_metrics':metric_array})
        return abns_ents

    def get_host_abnormal_entities(self, host_id):
        """
        查询主机所有的异常数据
        输入：
            host_id: 主机实体id
        输出：
            失败或没有数据返回空
            成功分类返回主机内所有异常实体和异常指标列表
            格式：
            {
                'thread': [<abn_entity>, ...],
                'proc': [<abn_entity>, ...],
                ...
            }
            abn_entity异常实体格式
            {
                entity: xxx, --> 实体信息
                abn_metrics: [<abn1>, <abn2>, ...], --> 异常指标信息列表
            }
            abn格式：
            {
                metrics: xxx
                desc: xxx
                event_id: xxx
            }
        """
        t = _parse_document_id(host_id)
        if not t: return []
        _, ts, machine_id, _ = t
        return self._get_abnormal_entities(ts, machine_id)

    def get_abnormal_metrics_by_id(self, eid):
        """
        查询实体对应的异常数据
        输入：
            eid: 实体id
        输出：
            没有异常数据返回None
            有异常返回异常指标列表
            格式：
            [<abn>, ...]
            abn格式：
            {
                metrics: xxx
                desc: xxx
                event_id: xxx
            }
        """
        t = _parse_document_id(eid)
        if not t: return None
        key, ts, machine_id, etype = t
        abns = self._get_abnormal_entities(ts, match = key)
        if not abns.get(etype):
            return None
        return abns[etype][0]['abn_metrics']

    def _get_root_cause_topo(self, root_event: dict):
        ts = root_event['Timestamp']
        coll = self._get_recent_collection(ts, DEFAULT_INTERVAL)
        if not coll: return None
        top1_path = root_event['Resource']['cause_metrics'][0]['path'] 
        spread_path = [
                (_document_id_by_coll(coll, e['entity_id']), 
                 _fetch_metric_name(e['metric_id']),
                 e['desc']) 
                for e in top1_path]

        machine_id = top1_path[0]['metric_labels']['machine_id']
        filters = {"machine_id":machine_id, 'type':'host'}
        rets = self.get_elements_by_filter(ts, filters=filters, ret_fields=['_id'])
        host_eid = rets[0]['_id']
        topo_paths = []
        aql_query = '''
        FOR v, e IN ANY SHORTEST_PATH
            "{start}" TO "{finish}" belongs_to, runs_on
            return v._id
        '''
        for v in spread_path:
            aql = aql_query.format(start = v[0], finish = host_eid)
            data = self._aql_query(DB_TOPO, aql, {})
            topo_paths.append(tuple(data))

        s1 = set(topo_paths)
        s2 = copy.deepcopy(s1)
        for v in s1:
            vs = set(v)
            for v2 in s2:
                vs2 = set(v2)
                if vs < vs2:
                    s2.remove(v)
                    break
        topo_paths = []
        for v in s2:
            tmp = []
            for v2 in v:
                tmp.append(self.get_element_by_id(v2))
            topo_paths.append(tmp)

        ret_dict = {
            '_id': root_event['_id'],
            '_key': root_event['_key'],
            'Timestamp': ts,
            "machine_id": machine_id,
            'SeverityNumber': root_event['SeverityNumber'],
            'SeverityText': root_event['SeverityText'],
            'Body': root_event['Body'],
            'topo_path': topo_paths,
            'spread_path': spread_path
        }
        return ret_dict

    def get_root_cause_topo_by_event_id(self, event_id: str):
        """
        通过异常事件id获取根因定位结果
        输入：
            event_id: 异常事件id, 对应异常中的event_id
        输出：
            失败或没有数据返回空
            成功返回根因定位结果
            根因定位结果格式：
            {
              '_id':  --> document id
              '_key': --> document key 
              '_rev': --> document version
              'Timestamp': --> <unix_time>时间戳
              'abnormal_metrics': {
                    'metrics': "<异常指标>"
                    'entity_id': "<异常实体>"
              }
              'root_cause_metrics': {
                    'metrics': "<根因指标>"
                    'entity_id': "<根因实体>"
              }
              'topo_path': [[<path>], [<path>], ...] --> topo路径
              'spread_path': [(entity_id, metric, desc), (entity_id, metric, desc), ...] --> 传播路径, 第一个元素表示根因指标，最后一个元素表示异常指标
              "machine_id": --> 主机machine_id
              'SeverityNumber': --> 异常严重等级编号
              'SeverityText': --> 异常等级描述(INFO/9 WARN/13 ERROR/17 FATAL/21)
              'Body': --> 异常描述
            }

            path格式:
            path列表表示一路径，列表的元素表示路径的节点，
            列表第一个和最后一个元素表示路径的起点和终点(host)
            [entity1, entity2, ..., host_entity]
        """
        root_event = self.get_element_by_id(event_id, DB_EVENT)
        if not root_event:
            return None
        return self._get_root_cause_topo(root_event)

    def get_root_cause_entries(self, start, end, interval = DEFAULT_INTERVAL):
        """
        输入：
            start: <unix_time>时间戳，开始时间点
            end: <unix_time>时间戳，结束时间点
            interval: 筛选重复根因topo的间隔(s)，即相同根因topo的间隔大于inetrval才会被记录
                    (异常entity_id与metrics作为key)
        输出：
            失败或没有数据返回空
            成功返回根因定位列表
            格式：
            [
                {
                  '_id':  --> document id
                  '_key': --> document key 
                  '_rev': --> document version
                  'Timestamp': --> <unix_time>时间戳
                  'abnormal_metrics': {
                        'metrics': "<异常指标>"
                        'entity_id': "<异常实体>"
                        'desc': "异常描述"
                        'labels': {标签值}
                  }
                  'root_cause_metrics': {
                        'metrics': "<根因指标>"
                        'entity_id': "<根因实体>"
                        'desc': "根因描述"
                        'labels': {标签值}
                  }
                  "machine_id": --> 主机machine_id
                  'SeverityNumber': --> 异常严重等级编号
                  'SeverityText': --> 异常等级描述(INFO/9 WARN/13 ERROR/17 FATAL/21)
                  'Body': --> 异常描述
                }
                ...
            ]
        """
        #  TODO: interval set to 1 for no filtering
        root_causes = self._get_abnormal_root_cause_entries(
                start, end, 1, False)
        data = []
        for e in root_causes:
            machine_id = e['Resource']['abnormal_kpi']['metric_labels']['machine_id']
            abn_entity = e['Resource']['abnormal_kpi']['entity_id']
            abn_metric = e['Resource']['abnormal_kpi']['metric_id']
            abn_desc = e['Resource']['abnormal_kpi']['desc']
            abn_labels = e['Resource']['abnormal_kpi']['metric_labels']
            root_entity = e['Resource']['cause_metrics'][0]['entity_id']
            root_metric = e['Resource']['cause_metrics'][0]['metric_id']
            root_desc = e['Resource']['cause_metrics'][0]['desc']
            root_labels = e['Resource']['cause_metrics'][0]['metric_labels']
            ret_dict = {
                '_id': e['_id'],
                '_key': e['_key'],
                'Timestamp': e['Timestamp'],
                "machine_id": machine_id,
                'SeverityNumber': e['SeverityNumber'],
                'SeverityText': e['SeverityText'],
                'Body': e['Body'],
                'abnormal_metrics': {
                    'metrics': _fetch_metric_name(abn_metric),
                    'entity_id': abn_entity,
                    'desc': abn_desc,
                    'labels': abn_labels,
                },
                'root_cause_metrics': {
                    'metrics': _fetch_metric_name(root_metric),
                    'entity_id': root_entity,
                    'desc': root_desc,
                    'labels': root_labels,
                }
            }
            data.append(ret_dict)
        return data

    def get_shortest_path(self, start_id: str, end_id: str) -> list:
        """
        获取start->end的单源最短路径
        输入：
            start_id: 起点实体id 
            end_id: 终点实体id
        输出：
            失败或start->end路径不可达返回空列表
            成功返回实体id路径列表
            格式：
            [
                '<start_id>',
                '<entity_id>',
                '<end_id>',
                ...
            ]
        """
        bind_vars = {'start': start_id, 'finish': end_id}
        aql_query = '''
        FOR v, e IN ANY SHORTEST_PATH
            @start TO @finish belongs_to, runs_on
            return v._id
        '''
        data = self._aql_query(DB_TOPO, aql_query, bind_vars)
        return data

    def get_values_by_label(self, timestamp = None, 
                               filters: dict = {}, label: str = '') -> list:
        """
        根据过滤条件筛选获取标签所有值集合
        输入：
            timestamp: <unix_time>时间戳，不提供timestamp默认当前系统时间
            filters: 过滤条件，多个过滤条件是and关系, 过滤字段参考观测实体属性
            label: 标签名称
        输出：
            失败或没有数据返回空列表
            成功返回标签值集合
            格式：
            [val1, val2, valN, ...]
        示例：
            获取host内所有进程名称
            filters = {'machine_id':'702c2aa458ff4e479e22b89a4a8cf53f', 'type':'proc'}
            res = get_values_by_label(filters=filters, label='comm')
            res:
                ["gala-gopher", "gaussdb", "java", "prometheus", "python", "python3", "su"]
        """
        coll = self._get_recent_collection(timestamp)
        if not coll:
            return []
        bind_vars = {'@COLL': coll}
        filter_str = _filter_options_format(filters)
        aql_query = '''
        FOR v IN @@COLL
            FILTER {}
            collect val = v.{}
            return val
        '''.format(filter_str, label)
        data = self._aql_query(DB_TOPO, aql_query, bind_vars)
        return data
