from dataclasses import dataclass
import os
import sys
import getopt
import time

INTERVAL = 5
DB_USER = "omm"
GAUSS_NAME = "gaussdb"
GAUSS_ROLE_KEY = "local_role"
GAUSS_STATE_KEY = "db_state"
GAUSS_CHANNEL_KEY = "channel"
GAUSS_BIND_IP_KEY = "local_bind_address"
GAUSS_BIND_PORT_KEY = "port"

CONTAINER_QUERY_PREFIX = "docker exec -u {user} {container_id} bash -c \"source ~/.bashrc;{subcmd}\""
HOST_QUERY_PREFIX = "su - {user} -c \"{subcmd}\""
QUERY_GAUSS_PROC_CMD = "ps -e -o pid,cmd | grep -v grep |grep \"{process}\""
QUERY_GAUSS_GSCTL_CMD = "gs_ctl query -D {db_path}"
QUERY_GAUSS_GSGUC_CMD = "gs_guc check -D {db_path}  -c \"{field}\""
QUERY_CONTAINER_ID_CMD = "docker container ls -q"
QUERY_CONTAINER_PID_CMD = "docker inspect -f '{{{{.State.Pid}}}}' {container_id}"
QUERY_CONTAINER_GAUSS_DN_PATH_CMD = "docker exec {container_id} printenv PGDATA"


@dataclass
class GaussInfo:
    db_name: str = 'openGauss'
    db_role: str = ''
    db_ip: str = ''
    db_port: str = ''
    db_path: str = ''
    db_state: str = ''
    pid: str = ''
    cmdline: str = ''
    container_id: str = ''
    status: bool = False


def _parser_proc_line(line):
    """
    "1234 gaussdb -D /var/lib/dn sss"
    """
    if not line or line.isspace():
        return None
    line = line.strip()
    p = line.partition(' ')  # ('1234', ' ', ' -D /var/lib/dn sss')
    pid = p[0]
    cmd = p[2].strip()  # 'gaussdb -D /var/lib/dn sss'
    if not pid.isalnum():
        return None
    dn = ''
    if "-D" in cmd:
        sp = cmd.split()  # ['gaussdb','-D', '/var/lib/dn', 'sss']
        if len(sp) > 2:
            dn = sp[2]
    return (pid, cmd, dn)


def _parser_gauss_gs_ctl(output):
    """
     HA state:
         local_role                     : Standby
         static_connections             : 1
         db_state                       : Normal
         detail_information             : Normal
         channel                        : 10.244.197.62:60128<--10.244.193.226:15401
    """
    if not output or output.isspace():
        return None
    role = ''
    state = ''
    ip = ''
    for line in output.split('\n'):
        sp = line.split(':')
        if len(sp) < 2:
            continue
        if GAUSS_ROLE_KEY in line:
            role = sp[1].strip()
        elif GAUSS_STATE_KEY in line:
            state = sp[1].strip()
        elif GAUSS_CHANNEL_KEY in line:
            ip = sp[1].strip()
    return (role, state, ip)


def _parser_gauss_gs_guc(output, key):
    """
    Total GUC values: 1. Failed GUC values: 0.
    The value of parameter local_bind_address is same on all instances.
        local_bind_address='10.244.197.62'
    """
    value = ''
    if not output or output.isspace():
        return None
    for line in output.split('\n'):
        if key in line:
            sp = line.split('=')
            if len(sp) < 2:
                continue
            value = sp[1]
    return value


def _get_container_info():
    # get active container id/pid/PGDATA(env variable)
    containers = []
    with os.popen(QUERY_CONTAINER_ID_CMD) as f:
        for l in f.readlines():
            cid = l.strip()
            with os.popen(QUERY_CONTAINER_PID_CMD.format(container_id=cid)) as f2:
                pid = f2.read().strip()
            cmd = QUERY_CONTAINER_GAUSS_DN_PATH_CMD.format(container_id=cid)
            with os.popen(cmd) as f3:
                dn = f3.read().strip()
            tup = (cid, pid, dn)
            containers.append(tup)
    return containers


def get_gauss_proc():
    with os.popen(QUERY_GAUSS_PROC_CMD.format(process=GAUSS_NAME)) as f:
        gausses = []
        for l in f.readlines():
            tup = _parser_proc_line(l)
            if tup:
                gauss = GaussInfo(pid=tup[0], cmdline=tup[1], db_path=tup[2])
                gausses.append(gauss)
        return gausses


def get_gauss_container(gauss_ins):
    containers = _get_container_info()
    for gauss in gauss_ins:
        for con in containers:
            if gauss.pid == con[1]:  # gaussdb maybe start with container
                # when proc cmdline without db_path update it with container env PGDATA
                if not gauss.db_path:
                    gauss.db_path = con[2]
                    gauss.container_id = con[0]


def exec_gauss_cmd(subcmd, container_id):
    output = ''
    if container_id:  # container startup
        cmd = CONTAINER_QUERY_PREFIX.format(user=DB_USER,
                                            container_id=container_id, subcmd=subcmd)
    else:  # host startup
        cmd = HOST_QUERY_PREFIX.format(user=DB_USER, subcmd=subcmd)
    with os.popen(cmd) as f:
        output = f.read()
    return output


def get_gauss_gsctl(gauss):
    gsctl_cmd = QUERY_GAUSS_GSCTL_CMD.format(db_path=gauss.db_path)
    gsctl_output = exec_gauss_cmd(gsctl_cmd, gauss.container_id)
    role, state, ip = _parser_gauss_gs_ctl(gsctl_output)
    gauss.db_role = role
    gauss.db_state = state
    gauss.db_ip = ip


def get_gauss_gsguc(gauss):
    #  gsguc_ip_cmd = QUERY_GAUSS_GSGUC_CMD.format(db_path = gauss.db_path, field = GAUSS_BIND_IP_KEY)
    #  guc_ip_output = exec_gauss_cmd(gsguc_ip_cmd, gauss.container_id)
    #  ip = _parser_gauss_gs_guc(guc_ip_output, GAUSS_BIND_IP_KEY)
    #  if 'NULL' not in ip:
    #      gauss.db_ip = ip.strip('\'')
    gsguc_port_cmd = QUERY_GAUSS_GSGUC_CMD.format(db_path=gauss.db_path, field=GAUSS_BIND_PORT_KEY)
    guc_port_output = exec_gauss_cmd(gsguc_port_cmd, gauss.container_id)
    port = _parser_gauss_gs_guc(guc_port_output, GAUSS_BIND_PORT_KEY)
    gauss.db_port = port


def query_gauss_info(gauss_ins):
    for gauss in gauss_ins:
        if not gauss.db_path:  # abnormal gaussdb instance
            continue
        get_gauss_gsctl(gauss)
        get_gauss_gsguc(gauss)
        if gauss.db_role and gauss.db_state:
            gauss.status = True


def parser_options():
    global INTERVAL
    global GAUSS_NAME
    global DB_USER
    opts, args = getopt.getopt(sys.argv[1:], "t:p:u:", ["interval=", "process=", "user="])
    for opt, arg in opts:
        if opt in ("-t", "--interval"):
            INTERVAL = int(arg)
        if opt in ("-p", "--process"):
            GAUSS_NAME = arg
        if opt in ("-u", "--user"):
            DB_USER = arg


def start_gauss_probe():
    gauss_ins = get_gauss_proc()
    get_gauss_container(gauss_ins)
    query_gauss_info(gauss_ins)
    for gauss in gauss_ins:
        if not gauss.status or not gauss.db_ip or not gauss.db_port:
            continue
        # |table_name|key|label|metric|
        print(f"|app_gauss|{gauss.db_ip}_{gauss.db_port}|{gauss.db_name}|{gauss.db_ip}"
              f"|{gauss.db_port}|{gauss.pid}|{gauss.db_role}|{gauss.db_state}|{gauss.container_id}|0|")
        sys.stdout.flush()


if __name__ == "__main__":
    parser_options()
    while True:
        time.sleep(INTERVAL)
        try:
            start_gauss_probe()
        except Exception as e:
            print("[gauss_probe]get metrics failed. Err: %s" % repr(e))
