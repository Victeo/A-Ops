from kafka import KafkaConsumer
import json
import threading


# enable_auto_commit=False时, 消费后CURRENT-OFFSET不会增加
# auto_offset_reset=latest启动时, CURRENT-OFFSET默认为最新处, 之前的记录不会被消费, 新的记录才会被消费, 影响首次获取数据
# enable_auto_commit=False, auto_offset_reset=earliest, 重启时总是能获取所有的数据
# enable_auto_commit=False, auto_offset_reset=latest, 总是默认current=end offset, 所以每次启动都是没有内容
# enable_auto_commit=True, auto_offset_reset=latest, 第一次时启动时到latest处(没有内容)，
#   后续每次启动都能获取end_offset - current_offset的全部内容, 且会更新current-offset
# enable_auto_commit=True, auto_offset_reset=earliest, 第一次时启动时到earliest(全部内容)，
#   后续每次启动都能获取end_offset - current_offset的全部内容, 且会更新current-offset
# 默认 enable_auto_commit=True, auto_offset_reset=latest

class KafkaCollector(threading.Thread):
    def __init__(self, server_ip: str = 'localhost', port: int = 9092,
            topics:list = None, group:str = None, callback: '<class function>' = None):
        """
        kafka消费者线程类
        server_ip: kafka服务器ip
        port: kafka端口号
        topics: kafka主题列表
        group: kafka消费组id, 默认不加入任何组
        callback: 消费者回调函数, 函数原型<callback(value, topic)， 
                  value的数据格式需参考订阅的topic
        """
        super().__init__()
        self.kafka_config = {
            "bootstrap_servers": f"{server_ip}:{port}",
            "group_id": group,
            "enable_auto_commit": True,
            "auto_offset_reset": "earliest",
            "value_deserializer": lambda m: json.loads(m.decode('utf-8')),
        }
        self.cb = callback
        try:
            self.consumer = KafkaConsumer(*topics, **self.kafka_config)
        except Exception as e:
            print(f'Connect to kafka {server_ip}:{port}:{topics} failed. {e}')

    def run(self):
        """
        线程回调函数
        """
        for message in self.consumer:
            self.cb(message.value, message.topic)
