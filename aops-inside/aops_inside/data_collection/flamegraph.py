import requests
import re
from datetime import datetime
from common.config import AopsConfigParser

FORMAT_TIME_STR = "%Y-%m-%d-%H-%M-%S"
FORMAT_TIME_TO_DAY_STR = "%Y-%m-%d"


def format_flamegraph_url(host, flame_type, ts):
    """
    格式化时间戳对应的火焰图url
    输入：
        host: 主机ip
        flame_type: 火焰图类型[oncpu|offcpu|io|memleak]
        ts: unix时间戳
    输出格式：
        'http://10.244.197.203:8123/10.244.198.169/oncpu/2022-11-11/2022-11-11-17-41-47.svg' 
    """
    config = AopsConfigParser()
    server_ip = config.fileserver_ip
    port = config.fileserver_port
    dt = datetime.fromtimestamp(ts)
    date = dt.strftime(FORMAT_TIME_STR)
    day = dt.strftime(FORMAT_TIME_TO_DAY_STR)
    url = f"http://{server_ip}:{port}/{host}/{flame_type}/{day}/{date}.svg"
    return url


def _flamegraph_url_prefix(url):
    """
    输入：
        'http://10.244.197.203:8123/10.244.198.169/oncpu/2022-11-11/2022-11-11-17-41-47.svg' 
    输出格式：
        'http://10.244.197.203:8123/10.244.198.169/oncpu/2022-11-11' 
    """
    return url[:url.rfind('/')]


def _get_flame_timestamp_list(url):
    try:
        response = requests.get(url, timeout=1)
    except Exception as e:
        print('<{}> get failed:{}'.format(url, e))
        return []
    content = response.content.decode('UTF-8')
    svgs = re.findall(r">(\d{4}-\d{2}-\d{2}-\d{2}-\d{2}-\d{2})\.svg<", content)
    ts_list = [int(datetime.strptime(ts, FORMAT_TIME_STR).timestamp()) for ts in svgs]
    return ts_list


def _get_recent_timestamp(ts, ts_arr):
    l = 0;
    r = len(ts_arr) - 1
    if r < 0 or ts < ts_arr[l]:
        return None
    if ts >= ts_arr[r]:
        return ts_arr[r]
    if ts == ts_arr[l]:
        return ts_arr[l]

    while l < r:
        m = (l + r) // 2
        if ts == ts_arr[m]:
            return ts_arr[m]
        elif ts > ts_arr[m]:
            l = m
        else:
            r = m

        if l + 1 == r:
            return ts_arr[l]
    return None


def get_flamegraph_url(host, flame_type, ts):
    """
    获取同一天内与时间戳ts最近的时间点的火焰图url
    不存在或失败返回None
    输入：
        host: 主机ip
        flame_type: 火焰图类型[oncpu|offcpu|io|memleak]
        ts: unix时间戳
    输出格式：
        (timestamp, url)
        'http://10.244.197.203:8123/10.244.198.169/oncpu/2022-11-11/2022-11-11-17-41-47.svg' 
    """
    query_url = _flamegraph_url_prefix(format_flamegraph_url(host, flame_type, ts))
    ts_list = _get_flame_timestamp_list(query_url)
    recent_ts = _get_recent_timestamp(ts, ts_list)
    if not recent_ts:
        return None
    #  TODO: backoff time
    return (recent_ts, format_flamegraph_url(host, flame_type, recent_ts))


def get_flamegraph_url_list(host, flame_type, ts):
    """
    获取时间戳ts当天的所有火焰图url列表
    不存在或失败返回空
    输入：
        host: 主机ip
        flame_type: 火焰图类型[oncpu|offcpu|io|memleak]
        ts: unix时间戳
    输出格式：
        [(timestamp, url), (timestamp, url), ...]
        [
            'http://10.244.197.203:8123/10.244.198.169/oncpu/2022-11-11/2022-11-11-17-41-47.svg', 
            'http://10.244.197.203:8123/10.244.198.169/oncpu/2022-11-11/2022-11-11-17-45-41.svg'
        ]
    """
    query_url = _flamegraph_url_prefix(format_flamegraph_url(host, flame_type, ts))
    ts_list = _get_flame_timestamp_list(query_url)
    svg_urls = []
    for t in ts_list:
        svg_urls.append((t, format_flamegraph_url(host, flame_type, t)))
    return svg_urls
