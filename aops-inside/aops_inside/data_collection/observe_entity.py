from dataclasses import dataclass
from data_collection.kafka_adpter import KafkaCollector
from common.config import AopsConfigParser

# from arangodb import _parse_document_id

ENTITY_DICT = {}


@dataclass
class ObserveEntity():
    etype: str
    keys: list
    labels: list
    metrics: list
    version: str = ''


def observe_metadata_cb(metadata, topic):
    etype = metadata.get('entity_name')
    if not etype:
        return
    keys = metadata.get('keys')
    if not keys:
        print(f"empty keys of entity {etype}!")
        return
    labels = metadata.get('labels')
    metrics = metadata.get('metrics')
    version = metadata.get('version')

    entity_obj = ObserveEntity(etype, keys, labels, metrics, version)
    old_entity_obj = ENTITY_DICT.get(etype)
    if not old_entity_obj:
        ENTITY_DICT.setdefault(etype, entity_obj)
        return

    # check keys consistent 
    old_keys = old_entity_obj.keys
    if len(old_keys) != len(keys):
        print(f"entity {etype} keys not consistent:{old_keys} <--> {keys}!")
        return
    for k1, k2 in zip(old_keys, keys):
        if k1 != k2:
            print(f"entity {etype} keys not consistent:{old_keys} <--> {keys}!")
            return

    # merge labels & metrics
    old_labels = old_entity_obj.labels
    old_metrics = old_entity_obj.metrics
    merged_labels = list(set(old_labels + labels))
    merged_metrics = list(set(old_metrics + metrics))
    old_entity_obj.labels = merged_labels
    old_entity_obj.metrics = merged_metrics


def get_observe_entity_by_type(etype):
    """
    通过实体类型获取实体详情
    输入: 
        etype实体类型
    输出:
        没有对应实体类型返回None
        成功返回实体详情对象<ObserveEntity>
    """
    if not len(ENTITY_DICT):
        print("No observe entities collected!")
    return ENTITY_DICT.get(etype)


def get_observe_entity_types():
    """
    返回所有实体类型字符串列表
    """
    if not len(ENTITY_DICT):
        print("No observe entities collected!")
    return list(ENTITY_DICT)

def start_metadata_thread():
    config = AopsConfigParser()
    topics = [config.kafka_topics['metadata']]
    t = KafkaCollector(config.kafka_ip, config.kafka_port, topics, callback=observe_metadata_cb)
    t.setDaemon(True)
    t.start()

# def get_observe_entity_key_pairs(eid):
#     """
#     解析实体id,返回实体keys的键值对
#     失败返回空
#     示例：
#         以tcp_link实体id为例
#         ObserveEntities_1661412026/ec28db3684174873b1b78b79700bc27c_tcp_link_2064741_1_10.244.197.62_10.244.197.203_52918_15401_2
#         {
#           "machine_id": "ec28db3684174873b1b78b79700bc27c",
#           "tgid": "2064741",
#           "role": "1",
#           "client_ip": "10.244.197.62",
#           "server_ip": "10.244.197.203",
#           "client_port": "52918",
#           "server_port": "15401",
#           "protocol": "2",
#         }
#     """
#     t = _parse_document_id(eid)
#     if not t: return {}
#     key, _, machine_id, etype = t
#     sp = key.split(etype)
#     keys = sp[1][1:].split('_')
#     keys[0:0] = [machine_id]
#     print(keys)
#     if etype not in ENTITY_DICT:
#         print(f"{etype} not in observe entity collection!")
#         return {}
#     ent_keys = ENTITY_DICT[etype].keys
#     if len(keys) != len(ent_keys):
#         print("keys not consistent!")
#         return {}
#     pairs = zip(ent_keys, keys)
#     return dict(pairs)
