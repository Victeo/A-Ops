import time
import random
from prometheus_client import Gauge, start_http_server

host_labels = ['os_version', 'hostname', 'kversion', 
              'cpu_num', 'memory_MB', 'ip_addr', 'host_type', 'machine_id']
host_value = Gauge('gala_gopher_host_value', 'prometheus python client', host_labels)

proc_labels = ['tgid', 'pgid', 'ppid', 'comm', 'cmdline', 'machine_id', 'uuid', 'vm_name']
proc_value = Gauge('gala_gopher_proc_value', 'prometheus python client', proc_labels)


if __name__ == '__main__':
    start_http_server(9999)  # 9999端口启动
    while True:
        #将上一次的值去除
        #  host.clear()
        print("write")
        #设置标签以及监控数据的值
        # physical host
        host_value.labels(os_version="openEuler-20.03-LTS-SP1", hostname="euler",
                    kversion="4.19.90-2012.4.0.0053.oe1", cpu_num="16", 
                    memory_MB="14920", ip_addr="10.244.197.203", host_type="pm",
                    machine_id="f954f3e6-c555-4d1b-972c-2a64f649c05b").set(1)

        # qemu-kvm proc
        # vm1 <---> gauss1
        proc_value.labels(tgid="1111", pgid="1", ppid="1",
                    comm="qemu-kvm", cmdline="qemu-kvm", vm_name="vm1",
                    uuid="88dfd3cf-460e-4cc3-aa7b-6896ee5dc400",
                    machine_id="f954f3e6-c555-4d1b-972c-2a64f649c05b").set(1)
        # vm2 <---> gauss2
        proc_value.labels(tgid="1112", pgid="1", ppid="1",
                    comm="qemu-kvm", cmdline="qemu-kvm", vm_name="vm2",
                    uuid="b1bed3bc-df96-4763-8bdc-2c384ab43d8f",
                    machine_id="f954f3e6-c555-4d1b-972c-2a64f649c05b").set(1)
        # vm3 <---> gauss3
        proc_value.labels(tgid="1113", pgid="1", ppid="1",
                    comm="qemu-kvm", cmdline="qemu-kvm", vm_name="vm3",
                    uuid="702c2aa4-58ff-4e47-9e22-b89a4a8cf53f",
                    machine_id="f954f3e6-c555-4d1b-972c-2a64f649c05b").set(1)

        # vhost proc
        proc_value.labels(tgid="2221", pgid="1", ppid="1",
                    comm="vhost-1111", cmdline="vhost-1111", vm_name="",
                    uuid="88dfd3cf-460e-4cc3-aa7b-6896ee5dc400",
                    machine_id="f954f3e6-c555-4d1b-972c-2a64f649c05b").set(1)
        proc_value.labels(tgid="2222", pgid="1", ppid="1",
                    comm="vhost-1112", cmdline="vhost-1112", vm_name="",
                    uuid="b1bed3bc-df96-4763-8bdc-2c384ab43d8f",
                    machine_id="f954f3e6-c555-4d1b-972c-2a64f649c05b").set(1)
        proc_value.labels(tgid="2223", pgid="1", ppid="1",
                    comm="vhost-1113", cmdline="vhost-1113", vm_name="",
                    uuid="702c2aa4-58ff-4e47-9e22-b89a4a8cf53f",
                    machine_id="f954f3e6-c555-4d1b-972c-2a64f649c05b").set(1)

        # ceph proc

        time.sleep(5)
