import time
from datetime import datetime

from django.http import HttpRequest, JsonResponse, HttpResponseNotAllowed, HttpResponseBadRequest

from common.comm import combine_description_name, get_abnormal_mapping, get_entity_ip
from common.comm import CONF
from common.comm import connect_arangodb, ArangodbCollector, get_page_info
from common.data_struct import RespondStatus, ArangoRelation
from common.data_struct import RespondResult
from views.base import AbnormalInfo, FaultTopoStruct, LevelInfo, BaseInfo
from views.http_response import ConnectToArangoError


def get_entity_related_abnormal_list(request: HttpRequest):
    """
        data struct:
        {
            status: success/failure
            data:
            {
                totalNum:                   数据总数
                currentPage:                当前页码
                info:                       当前页内容
                [
                    {
                        id:                 异常检测的事件id
                        timeStamp:          异常出现的时间戳，毫秒级
                        IP:                 发生异常的主机IP
                        faultSource:        异常事件的根因源
                        faultSymptom:     出现异常的指标
                        desc:               异常描述信息
                    }, ...
                ]
            }
        }
        """
    res = RespondResult()
    if request.method != 'POST':
        return HttpResponseNotAllowed("Wrong request method.")
    event_id_list = request.POST['eventIdList']
    page_num = int(request.POST.get('currentPage', 1))
    page_size = int(request.GET.get('pageSize', 10))
    if not isinstance(event_id_list, list):
        return HttpResponseBadRequest("Invalid params.")

    res.status = str(RespondStatus.SUCCESS)
    if not event_id_list:
        return HttpResponseBadRequest("Invalid params.")
    arango = connect_arangodb(CONF.arangodb_db['spider'])
    res.data = {"totalNum": 0, "currentPage": 0, "info": None}
    data = []
    for event_id in event_id_list:
        ret = arango.get_root_cause_topo_by_event_id(event_id)
        info = AbnormalInfo(time_stamp=int(ret['Timestamp']) * 1000, level=ret['SeverityText'])
        info.id = ret['_id']
        info.IP = get_entity_ip(ret['machine_id'])
        info.faultSymptom = ret['abnormal_metrics']['desc']
        info.desc = ret['Body']
        info.faultSource = ret['root_cause_metrics']['desc']
        data.append(info.__dict__)

    res.data['totalNum'], res.data['info'], res.data['currentPage'] = get_page_info(data,
                                                                                    page_num,
                                                                                    page_size)
    return JsonResponse(res.__dict__)


def is_entity_related_fault(cur_info: str, key_word: str):
    if not key_word:
        return False
    return key_word not in cur_info


def get_abnormal_list_by_time_range(request: HttpRequest):
    """
    data struct:
    {
        status: success/failure
        data:
        {
            totalNum:                   数据总数
            currentPage:                当前页码
            info:                       当前页内容
            [
                {
                    id:                 异常检测的事件id
                    timeStamp:          异常出现的时间戳，毫秒级
                    IP:                 发生异常的主机IP
                    faultSource:        异常事件的根因源
                    faultSymptom:     出现异常的指标
                    desc:               异常描述信息
                }, ...
            ]
        }
    }
    """
    res = RespondResult()

    if request.method != 'GET':
        return HttpResponseNotAllowed("Wrong request method.")

    # 请求未指定时间范围时默认查询五分钟内的异常信息
    try:
        end = int(str(request.GET["end"])[:10])
    except:
        end = int(time.time())

    try:
        start = int(str(request.GET["start"])[:10])
    except:
        start = end - CONF.fault_interval

    try:
        page_num = int(request.GET.get('currentPage', 1))
    except:
        page_num = 1

    try:
        page_size = int(request.GET.get('pageSize', 10))
    except:
        page_size = 10

    keywords = request.GET.get('keywords', None)
    ip = request.GET.get('ip', None)
    process_id = request.GET.get('tgid', None)
    machine_id = request.GET.get('machineId', None)
    try:
        arango = connect_arangodb(CONF.arangodb_db['spider'])
    except:
        return ConnectToArangoError()

    ret = arango.get_root_cause_entries(start=start, end=end)

    res.data = {"totalNum": 0, "currentPage": 0, "info": None}
    res.status = str(RespondStatus.SUCCESS)
    if not ret:
        return JsonResponse(res.__dict__)

    data = []
    ip_mapping = {}
    for item in ret:
        if not ip_mapping.get(item['machine_id']):
            ip_mapping[item['machine_id']] = get_entity_ip(item['machine_id'])
        tgid = item["abnormal_metrics"]['labels'].get('tgid', '-1')
        if process_id and tgid != process_id:
            continue
        if machine_id and item['machine_id'] != machine_id:
            continue
        if is_entity_related_fault(ip_mapping[item['machine_id']], ip):
            continue
        if is_entity_related_fault(item['abnormal_metrics']['desc'], keywords) and \
                is_entity_related_fault(item['root_cause_metrics']['desc'], keywords):
            continue
        info = AbnormalInfo(time_stamp=int(item['Timestamp']) * 1000, level=item['SeverityText'])
        info.id = item['_id']
        # 获取主机IP地址
        info.IP = ip_mapping[item['machine_id']]
        info.machineID = item['machine_id']
        info.tgid = tgid
        info.faultSymptom = item['abnormal_metrics']['desc']
        info.desc = item['Body']
        info.faultSource = item['root_cause_metrics']['desc']
        data.append(info.__dict__)
    res.data['totalNum'], res.data['info'], res.data['currentPage'] = get_page_info(data,
                                                                                    page_num,
                                                                                    page_size)
    return JsonResponse(res.__dict__)


def set_app_fault_info(arango: ArangodbCollector, proc_info: BaseInfo, path: list, level: list,
                       spread: list):
    app_entity = arango.get_out_edges(proc_info.id, str(ArangoRelation.BELONGS_TO), 'app')
    if not app_entity:
        return proc_info
    app_entity = app_entity[0]
    app_info = BaseInfo(name=combine_description_name(app_entity))
    app_info.id = app_entity['_id']
    proc_info.appId = app_info.id
    app_info.key = app_entity['_key']
    app_info.status = proc_info.status
    app_info.type = app_entity['type']
    level.append(app_info.__dict__)
    topo_path = [app_info.__dict__, proc_info.__dict__]
    if topo_path not in path:
        path.append(topo_path)
    spread_path = (app_entity['_id'], '')
    if spread_path not in spread:
        spread.append((app_entity['_id'], ''))
    return proc_info


def fault_topos(request: HttpRequest):
    """
        data struct:
        {
            status: success/failure
            data:
            {
                topoPath：    topo路径
                spreadPath：  根因传播路径
                level:
                {
                    host: [],
                    resource: [],
                    sys: [],
                    proc: [],
                    app: [],
                }
            }
        }
        """
    res = RespondResult()

    try:
        event_id = request.POST['id']
    except:
        return HttpResponseBadRequest("Invalid params.")

    try:
        arango = connect_arangodb(CONF.arangodb_db['spider'])
    except:
        return ConnectToArangoError()

    ret = arango.get_root_cause_topo_by_event_id(event_id)
    level_info = LevelInfo()
    level_info.host = []
    topo_path = []
    abnormal_dict = get_abnormal_mapping(arango)
    # 暂时屏蔽传播路径的中的metric名称，功能扩展留用
    spread_path = []
    for item in ret['spread_path']:
        if isinstance(item, tuple) and len(item) > 2:
            spread_path.append((item[0], item[2]))
        else:
            spread_path.append(item)

    for path in ret['topo_path']:
        cur_path = []
        for index in range(len(path)):
            entity = BaseInfo(name=combine_description_name(path[index]))
            entity.id = path[index]['_id']
            entity.key = path[index]['_key']
            entity.status = False if abnormal_dict.get(path[index]['_key']) else True
            entity.type = path[index]['type']
            entity.IP = get_entity_ip(path[index]['machine_id'])
            if entity.type == 'proc':
                entity = set_app_fault_info(arango, entity, topo_path, level_info.app, spread_path)
            entity_info = entity.__dict__
            if entity.type in ('proc', 'thread', 'container', 'tcp_link', 'endpoint',
                               'runtime', 'sli') and entity_info not in level_info.proc:
                level_info.proc.append(entity_info)
            elif entity.type == 'app' and entity_info not in level_info.proc:
                level_info.app.append(entity_info)
            elif entity.type in ('nic', 'qdisc', 'disk',
                                 'block', 'cpu', 'mem') and entity_info not in level_info.proc:
                level_info.resource.append(entity_info)
            elif entity.type in ('fs', 'net', 'sched') and entity_info not in level_info.proc:
                level_info.sys.append(entity_info)
            elif entity.type == 'host' and entity_info not in level_info.proc:
                level_info.host.append(entity_info)
            cur_path.append(entity_info)

        topo_path.append(cur_path)
    fault_topo = FaultTopoStruct(topo=topo_path, spread=spread_path)
    fault_topo.level = level_info.__dict__
    res.data = fault_topo.__dict__
    res.status = str(RespondStatus.SUCCESS)

    return JsonResponse(res.__dict__)
