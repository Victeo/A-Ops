from django.http import HttpRequest, JsonResponse, HttpResponseBadRequest, HttpResponseNotAllowed
from django.shortcuts import render
from common.data_struct import RespondStatus
from common.data_struct import RespondResult
from common.comm import connect_arangodb, get_abnormal_mapping, ArangodbCollector
from common.comm import CONF
from views.http_response import ConnectToArangoError


def index(request: HttpRequest):
    return render(request, 'index.html')


def get_host_abnormal_num(arango: ArangodbCollector, host_list: list):
    count = 0
    for host in host_list:
        if arango.get_host_abnormal_entities(host['_id']):
            count += 1
    return count


def get_other_abnormal_num(arango: ArangodbCollector, entity_type: str):
    count = 0
    entities = arango.get_elements_by_filter(filters={'type': entity_type})
    abnormal_dict = get_abnormal_mapping(arango)
    for item in entities:
        if abnormal_dict.get(item['_key']):
            count += 1
    return count


def get_cluster_abnormal_num(arango: ArangodbCollector, clusters: list):
    count = 0
    for cluster in clusters:
        for edge in cluster:
            if edge.get('_from'):
                machine_id = edge['_from']['machine_id']
                host_info = arango.get_elements_by_filter(filters={'type': 'host',
                                                                   'machine_id': machine_id})[0]
                if arango.get_host_abnormal_entities(host_id=host_info['_id']):
                    count += 1
                    break
            if edge.get('_to'):
                machine_id = edge['_from']['machine_id']
                host_info = arango.get_elements_by_filter(filters={'type': 'host',
                                                                   'machine_id': machine_id})[0]
                if arango.get_host_abnormal_entities(host_id=host_info['_id']):
                    count += 1
                    break
    return count


def get_overview(request: HttpRequest):
    res = RespondResult()

    if request.method != 'GET':
        return HttpResponseNotAllowed("Wrong request method.")

    try:
        entity_type = request.GET['type']
    except:
        return HttpResponseBadRequest("Invalid params.")

    try:
        arango = connect_arangodb(CONF.arangodb_db['spider'])
    except:
        return ConnectToArangoError()

    abnormal_num = 0
    if entity_type == 'cluster':
        # 暂不获取集群数量
        ret = []
        abnormal_num = get_cluster_abnormal_num(arango, ret)
    else:
        ret = arango.get_elements_by_filter(filters={'type': entity_type})
        if entity_type == 'host':
            abnormal_num = get_host_abnormal_num(arango, ret)
        elif entity_type != 'database':
            abnormal_num = get_other_abnormal_num(arango, str(entity_type))

    # 异常获取待补齐
    res.data = {'totalNum': len(ret), 'abnormalNum': abnormal_num}
    res.status = str(RespondStatus.SUCCESS)
    return JsonResponse(res.__dict__)
