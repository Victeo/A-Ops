import configparser

from django.http import HttpRequest, JsonResponse, HttpResponseNotAllowed, HttpResponseBadRequest
from inside_backend.settings import METRICS_CONF
from common.data_struct import RespondStatus, RespondResult
from common.config import parse_bool_config
from common import custom_config
from data_collection.observe_entity import get_observe_entity_types, get_observe_entity_by_type


def save_custom_config(request: HttpRequest):
    """
    用于更新单个主机自定义配置信息
    """
    res = RespondResult()

    if request.method != "POST":
        return HttpResponseNotAllowed("Wrong request method.")

    try:
        machine_id = request.POST["machineId"]
        entity_type = request.POST['type']
        metric_name = request.POST['option']
        value = int(request.POST['value'])
    except:
        return HttpResponseBadRequest("Invalid params.")

    custom_config.set_value(str(machine_id), str(entity_type), str(metric_name), bool(value))

    res.status = str(RespondStatus.SUCCESS)

    return JsonResponse(res.__dict__)


def save_global_config(request: HttpRequest):
    """
    在metrics.conf文件中更新指定类型及metric的配置信息
    """
    res = RespondResult()

    if request.method != "POST":
        HttpResponseNotAllowed("Wrong request method.")

    try:
        entity_type = request.POST['type']
        metric_name = request.POST['option']
        value = int(request.POST['value'])
    except:
        HttpResponseBadRequest("Request's params is wrong.")
    config = configparser.ConfigParser()
    config.read(METRICS_CONF)
    # 若配置文件中不存在该section，则进行新增
    sections = config.sections()
    section = f"Metrics-{entity_type}"
    if section not in sections:
        config.add_section(section)
    # 更新配置文件
    if value:
        config.set(section, str(metric_name), str(bool(value)))
    else:
        config.remove_option(section, str(metric_name))

    if not config.options(section):
        config.remove_section(section)
    config.write(open(METRICS_CONF, "w"))
    res.status = str(RespondStatus.SUCCESS)
    return JsonResponse(res.__dict__)


def get_file_config(entity_type: str):
    """
    从metrics.conf文件中读取并返回指定实体类型的metric字段及展示配置
    """
    metrics = get_observe_entity_by_type(entity_type).metrics
    res = {}
    for metric in metrics:
        value = parse_bool_config(METRICS_CONF, f"Metrics-{entity_type}", metric)
        res[metric] = value

    return res


def display_metrics_total_type_list(request: HttpRequest):
    """
    返回所有实体类型
    data struct:
    {
        status: success/failure,                        请求响应的状态
        data:[]                                         全部实体类型列表
    }
    """
    res = RespondResult()
    if request.method != "GET":
        HttpResponseNotAllowed("Wrong request method.")

    data = get_observe_entity_types()
    res.data = []
    for item in data:
        if get_observe_entity_by_type(item).metrics:
            res.data.append(item)
    if res.data:
        res.status = str(RespondStatus.SUCCESS)

    return JsonResponse(res.__dict__)


def display_global_config(request: HttpRequest):
    """
    返回metrics.conf文件中指定类型的展示配置
    data_struct:
    {
        status:                         响应成功或失败的标志
        data:
        {
            metric: Y/N             是否展示某个字段的折线图
            ,...
        }
    }
    """
    res = RespondResult()
    if request.method != "GET":
        return HttpResponseNotAllowed("Wrong request method.")

    try:
        entity_type = str(request.GET["type"])
    except:
        return HttpResponseBadRequest("Invalid params.")

    res.data = get_file_config(entity_type)

    res.status = str(RespondStatus.SUCCESS)
    return JsonResponse(res.__dict__)


def display_custom_config(request: HttpRequest):
    """
    返回指定主机的指定类型的个性化配置信息，若没有，则从metrics.conf读取并返回
        data_struct:
        {
            status:                         响应成功或失败的标志
            data:
            {
                metric: Y/N             是否展示某个字段的折线图
                ,...
            }
        }
    """
    res = RespondResult()

    if request.method != 'POST':
        return HttpResponseNotAllowed("Wrong request method.")

    try:
        machine_id = request.POST['machineId']
        entity_type = str(request.POST['type'])
    except:
        return HttpResponseBadRequest("Invalid params.")

    res.status = str(RespondStatus.SUCCESS)
    # 读取全局默认配置
    res.data = get_file_config(entity_type)
    # 同步指定主机的个性化配置
    for metric in res.data.keys():
        custom_value = custom_config.get_value(str(machine_id), entity_type, metric)
        if custom_value is not None:
            res.data[metric] = custom_value

    return JsonResponse(res.__dict__)
