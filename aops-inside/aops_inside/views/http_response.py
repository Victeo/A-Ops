from django.http import HttpResponse


class ConnectToArangoError(HttpResponse):
    status_code = 600


class ConnectToPrometheusError(HttpResponse):
    status_code = 601
