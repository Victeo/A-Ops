class BaseInfo:
    def __init__(self, name: str):
        self.name = name
        self.id = ''
        self.key = ''
        self.status = True


class LevelInfo:
    def __init__(self):
        self.resource = []
        self.sys = []
        self.proc = []
        self.app = []


class SummaryInfo(LevelInfo):
    def __init__(self):
        LevelInfo.__init__(self)
        self.status = True
        self.edges = []


class HostList(BaseInfo):
    def __init__(self, name: str = ''):
        BaseInfo.__init__(self, name)
        self.machineId = ''
        self.IP = ''
        self.type = ''


class HostInfo(BaseInfo):
    def __init__(self, name: str = ''):
        BaseInfo.__init__(self, name)
        self.OS = 'openEuler'
        self.kernelVersion = '5.10.0-18.0.50.oe2203.x86_64'
        self.containerVersion = '18.09.0'
        self.IP = '127.0.0.1'
        self.specification = '4C 16G'
        self.children = []
        self.machineId = ''


class DetailBase:
    def __init__(self):
        self.info = []
        self.metricInfo = {'abnormal': None, 'normal': None}


class HorizonBase:
    def __init__(self, ip: str = '', stat: bool = True,
                 name: str = '', entity_id: str = '', key: str = ''):
        self.IP = ip
        self.status = stat
        self.name = name
        self.id = entity_id
        self.key = key


class AppInfo(HorizonBase):
    def __init__(self, host_id: str = '', app_id: str = '',
                 role: str = '', stat: bool = True, ip: str = '', pid: str = '',
                 port: str = '', app_name: str = '', key: str = '', hostname: str = ''):
        HorizonBase.__init__(self, ip, stat, app_name, app_id, key)
        self.hostId = host_id
        self.hostname = hostname
        self.pid = pid
        self.port = port
        self.role = role


class ProcInfo(HorizonBase):
    def __init__(self, host_id: str = '', ip: str = '', stat: bool = True,
                 proc_id: str = '', proc_name: str = '', key: str = '',
                 pid: str = '', hostname: str = ''):
        HorizonBase.__init__(self, ip, stat, proc_name, proc_id, key)
        self.hostId = host_id
        self.hostname = hostname
        self.pid = pid


class AbnormalInfo:
    def __init__(self, time_stamp: int = 0, level: str = ''):
        self.timeStamp = time_stamp
        self.IP = ''
        self.faultSource = ''
        self.faultSymptom = ''
        self.desc = ''
        self.id = ''
        self.level = level


class DataRate:
    def __init__(self, metric_name: str = ''):
        self.metricName = metric_name
        self.timeStamp = []
        self.values = []


class AbnormalRate(DataRate):
    def __init__(self, metric_name: str = '', desc: str = '', event_id: str = ''):
        DataRate.__init__(self, metric_name)
        self.desc = desc
        self.eventId = event_id


class FaultTopoStruct:
    def __init__(self, topo: list = None, spread: list = None):
        self.level = None
        self.topoPath = topo
        self.spreadPath = spread
