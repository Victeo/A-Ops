import time

from collections import OrderedDict
from django.http import HttpRequest, JsonResponse, HttpResponseBadRequest, HttpResponseNotAllowed

from common.comm import get_abnormal_mapping
from common.comm import connect_arangodb, ArangodbCollector, CONF
from common.data_struct import RespondStatus, HorizontalTopoType
from common.data_struct import RespondResult
from views.base import AppInfo, ProcInfo, HorizonBase
from views.http_response import ConnectToArangoError


def get_node_info(arango: ArangodbCollector, abnormal_dict: dict, entity: dict):
    res = None
    ret = arango.get_elements_by_filter(filters={'type': 'host',
                                                 'machine_id': entity['machine_id']})
    if not ret:
        return None
    host_info = ret[0]
    ip_addr = host_info['ip_addr'].split()[0]
    event_id_list = abnormal_dict.get(entity['_key'])
    status = False if event_id_list else True
    if entity['type'] == str(HorizontalTopoType.APP):
        res = AppInfo(app_id=entity['_id'], role=entity['role'], host_id=host_info['_id'],
                      stat=status, ip=entity['ip'], port=entity['port'], key=entity['_key'],
                      hostname=host_info['hostname'], app_name=entity['app_name'],
                      pid=entity['pid'])
    elif entity['type'] == str(HorizontalTopoType.HOST):
        res = HorizonBase(ip=ip_addr, key=entity['_key'], stat=status, name=entity['hostname'],
                          entity_id=entity['_id'])
    elif entity['type'] == str(HorizontalTopoType.PROC):
        res = ProcInfo(host_id=host_info['_id'], hostname=host_info['hostname'], ip=ip_addr,
                       pid=entity['tgid'], proc_name=entity['comm'], proc_id=entity['_id'],
                       stat=status, key=entity['_key'])
    else:
        return res

    return res.__dict__


def get_horizon_topo_info(arango: ArangodbCollector, edge: dict, abnormals: dict,
                          entities: OrderedDict):
    info = {'from': None, 'to': None}
    if edge.get('_from'):
        info['from'] = get_node_info(arango, abnormals, entities[edge['_from']])
    if edge.get('_to'):
        info['to'] = get_node_info(arango, abnormals, entities[edge['_to']])
    return info


def get_timestamp_entities(arango: ArangodbCollector, time_stamp: int, entity_type: str):
    res = OrderedDict()
    for item in arango.get_elements_by_filter(time_stamp, {'type': entity_type}):
        res.setdefault(item['_key'], {**item, })
    return res


def get_horizontal_topo(request: HttpRequest):
    """
    data struct:
        data:
        {
            current: []   存放当前时刻所查类型的数据的水平topo图，用实线画
            extinct: []   存放两小时前所查类型数据的水平topo图，用虚线表示
        }
        status：success/failure
    """
    res = RespondResult()

    if request.method != 'GET':
        return HttpResponseNotAllowed("Wrong request method.")
    try:
        topo_type = str(request.GET['type'])
    except:
        topo_type = str(HorizontalTopoType.APP)

    if topo_type not in ('app', 'proc', 'host'):
        return HttpResponseBadRequest("Invalid params.")

    try:
        arango = connect_arangodb(CONF.arangodb_db['spider'])
    except:
        return ConnectToArangoError("Can't connect to ArangoDB.")

    entity_event_dict = get_abnormal_mapping(arango)
    cur_time = int(time.time())
    last_time = cur_time - 7200
    last_entities = get_timestamp_entities(arango, last_time, topo_type)
    cur_entities = get_timestamp_entities(arango, cur_time, topo_type)
    before_topo = arango.get_horizontal_level_topo(timestamp=last_time, filters={'type': topo_type})
    cur_topo = arango.get_horizontal_level_topo(filters={'type': topo_type})

    res.status = str(RespondStatus.SUCCESS)

    extinct = []
    for edge in before_topo:
        if edge not in cur_topo:
            extinct.append(get_horizon_topo_info(arango, edge, entity_event_dict, last_entities))

    current = []
    for edge in cur_topo:
        current.append(get_horizon_topo_info(arango, edge, entity_event_dict, cur_entities))

    res.data = {'extinct_edges': extinct, 'current': current}

    return JsonResponse(res.__dict__)
