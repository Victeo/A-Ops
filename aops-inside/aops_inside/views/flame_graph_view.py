import json
from django.http import HttpRequest, JsonResponse, HttpResponseBadRequest, HttpResponseNotAllowed

from common.data_struct import RespondStatus
from common.data_struct import RespondResult
from data_collection.flamegraph import get_flamegraph_url_list


def get_flame_graph_by_day(request: HttpRequest):
    """
    根据指定主机和指定类型，获取指定日期的全部火焰图
        data struct:
            {
                status: success/failure,                                    请求响应的状态
                data:                                                       火焰图url列表
                    [
                        {
                            type:                                           火焰图类型
                            info:
                            [
                                {
                                    datetime: 2022-11-05 07:20:30           火焰图生成时间
                                    url:                                    火焰图文件
                                },...
                            ]
                        },...
                    ]
            }
    """
    res = RespondResult()

    if request.method != "POST":
        return HttpResponseNotAllowed("Wrong request method.")

    try:
        date_time = int(str(request.POST['datetime'])[:10])
        ip = request.POST['IP']
        flame_type = json.loads(request.POST['type'])
    except:
        return HttpResponseBadRequest("Invalid params.")

    if not isinstance(flame_type, list):
        return HttpResponseBadRequest("Invalid type of params.")

    res.data = []
    for current_type in flame_type:
        ret = get_flamegraph_url_list(ip, current_type, date_time)
        data = {'type': current_type, 'info': []}
        for timestamp, url in ret:
            data['info'].append({'datetime': int(timestamp) * 1000, 'url': url})
        res.data.append(data)

    res.status = str(RespondStatus.SUCCESS)
    return JsonResponse(res.__dict__)
