import json
import time
import math

from django.http import HttpRequest, JsonResponse, HttpResponseNotAllowed, HttpResponseBadRequest

from common import custom_config
from common.comm import connect_prometheus, get_abnormal_mapping, ArangodbCollector, get_page_info
from common.comm import connect_arangodb, combine_description_name, CONF
from common.config import parse_bool_config
from data_collection.observe_entity import get_observe_entity_by_type
from inside_backend.settings import METRICS_CONF

from common.data_struct import RespondStatus
from common.data_struct import RespondResult
from common.data_struct import ArangoRelation
from views.base import HostList, HostInfo, SummaryInfo, BaseInfo
from views.base import DetailBase, AbnormalRate, DataRate
from views.http_response import ConnectToArangoError

CHILD_TYPES = {
    'nic': 'dev_name',
    'cpu': 'cpu',
    'fs': 'MountOn',
    'container': 'name',
    'disk': 'disk_name',
    'proc': 'comm',
    'mem': 'infos',
    'net': 'origin',
    'app': 'app_name'
}


def get_host_list(request: HttpRequest):
    """
    data strcut:
    {
        status: success/failure
        data:
        {
            totalNUm:                   数据总数
            totalHost:                  主机总数
            abnormalHost:               异常主机总数
            normalHost:                 正常主机总数
            currentPage:                当前页码
            info:                       当前页内容
            [
                {
                    IP:                 主机IP
                    id:                 主机实体id
                    name:               主机名
                    status:             主机状态
                },...
            ]
        }
    }
    """
    res = RespondResult()

    if request.method != 'GET':
        return HttpResponseNotAllowed("Wrong request method.")
    status = request.GET.get('status', None)
    if status:
        status = True if status == 'true' else False
    hostname = request.GET.get('name', None)
    ip = request.GET.get('ip', None)
    page_num = int(request.GET.get('currentPage', 1))
    page_size = int(request.GET.get('pageSize', 10))
    # 获取主机列表
    try:
        arango = connect_arangodb(CONF.arangodb_db['spider'])
    except:
        return ConnectToArangoError()

    query_keys = {'type': 'host'}
    host_list = arango.get_elements_by_filter(filters=query_keys)
    if not host_list:
        return ConnectToArangoError()

    res.status = str(RespondStatus.SUCCESS)
    res.data = {"totalNum": 0, "currentPage": 0, "info": None,
                "totalHost": 0, "abnormalHost": 0, "normalHost": 0}
    data = []
    for item in host_list:
        res.data["totalHost"] += 1
        cur_status = False if arango.get_host_abnormal_entities(item['_id']) else True
        if cur_status:
            res.data["normalHost"] += 1
        else:
            res.data["abnormalHost"] += 1
        if status is not None and cur_status != status:
            continue
        if hostname and hostname not in item['hostname']:
            continue
        if ip and ip not in item['ip_addr'].split()[0]:
            continue
        host_list_info = HostList(item['hostname'])
        host_list_info.machineId = item['machine_id']
        # arango暂时无法查出主机IP
        host_list_info.IP = item['ip_addr'].split()[0]
        # 查询主机是否存在异常信息
        host_list_info.status = cur_status
        host_list_info.id = item['_id']
        host_list_info.key = item['_key']
        if item['host_type'] == 'vm':
            host_list_info.type = 'GuestOS'
        else:
            host_list_info.type = 'DiskOS' if is_disk_os(arango, item['_id']) else 'HostOS'
        data.append(host_list_info.__dict__)

    res.data['totalNum'], res.data['info'], res.data['currentPage'] = get_page_info(data,
                                                                                    page_num,
                                                                                    page_size)

    return JsonResponse(res.__dict__)


def get_host_topo(request: HttpRequest):
    res = RespondResult()
    if request.method != "POST":
        return HttpResponseNotAllowed("Wrong request method.")

    try:
        entity_id = str(request.POST['id'])
    except:
        return HttpResponseBadRequest("Invalid params.")

    arango = connect_arangodb(CONF.arangodb_db["spider"])
    ret = arango.get_element_by_id(eid=entity_id)
    if not ret:
        return ConnectToArangoError()
    host_info = HostInfo(ret['hostname'])
    host_info.id = entity_id
    host_info.key = ret['_key']
    host_info.machineId = ret['machine_id']
    host_info.IP = ret['ip_addr'].split()[0]
    host_info.OS = ret['os_version']
    host_info.kernelVersion = ret['kversion']
    host_info.specification = f"{ret['cpu_num']}C {math.ceil(int(ret['memory_MB']) / 1024)}G"
    host_info.status, host_info.children = get_summary_info(arango, host_info.id)
    host_info.children['os'] = get_host_relations(arango, entity_id, ret['host_type'])
    res.status = str(RespondStatus.SUCCESS)
    res.data = host_info.__dict__

    return JsonResponse(res.__dict__)


def get_host_relations(arango: ArangodbCollector, entity_id: str, host_type: str):
    res = {}
    # 以虚拟机为原点查询，GUEST --out--> host --out--> disk
    if host_type == "vm":
        res['hostOS'] = get_hosts_info(arango, entity_id, str(ArangoRelation.RUNS_ON), False)
        res['diskOS'] = []
        for item in res['hostOS']:
            res['diskOS'].extend(get_hosts_info(arango, item['id'],
                                                str(ArangoRelation.RUNS_ON), False))
        return res
    # 以磁阵主机为原点查询，guest <--in-- host <--in-- DISK
    if is_disk_os(arango, entity_id):
        res['hostOS'] = get_hosts_info(arango, entity_id, str(ArangoRelation.STORE_IN), True)
        res['guestOS'] = {}
        for item in res['hostOS']:
            res['guestOS'][item['key']] = get_hosts_info(arango, item['id'],
                                                         str(ArangoRelation.RUNS_ON), True)
    # 以宿主机为原点查询, guest <--in-- HOST --out--> disk
    else:
        res['guestOS'] = get_hosts_info(arango, entity_id, str(ArangoRelation.RUNS_ON), True)
        res['diskOS'] = get_hosts_info(arango, entity_id, str(ArangoRelation.RUNS_ON), False)

    return res


def is_disk_os(arango: ArangodbCollector, entity_id: str):
    ret = arango.get_out_edges(start=entity_id, relation_type=str(ArangoRelation.STORE_IN),
                               entity_type='host')
    return ret is None


def get_hosts_info(arango: ArangodbCollector, entity_id: str, relation: str, is_in: bool):
    if is_in:
        ret = arango.get_in_edges(start=entity_id, relation_type=relation, entity_type='host')
    else:
        ret = arango.get_out_edges(start=entity_id, relation_type=relation, entity_type='host')
    data = []
    for item in ret:
        host = BaseInfo(item['hostname'])
        host.id = item['_id']
        host.key = item['_key']
        host.status = False if arango.get_host_abnormal_entities(item['_id']) else True
        data.append(host.__dict__)
    return data


def get_summary_info(arango: ArangodbCollector, entity_id: str):
    status = True
    summary_info = SummaryInfo()
    abnormal_dict = get_abnormal_mapping(arango, entity_id)
    entity = arango.get_element_by_id(entity_id)
    machine_id = entity['machine_id']
    time_stamp = entity['timestamp']

    for child_type in CHILD_TYPES.keys():
        query_dict = {'machine_id': machine_id, 'type': child_type}
        ret = arango.get_elements_by_filter(time_stamp, query_dict)
        if not ret:
            continue
        data = []
        # 获取主机相关的全部实体信息
        for item in ret:
            # 设置name字段的值
            if child_type == 'cpu':
                cpu = item[CHILD_TYPES[child_type]]
                if cpu == 'cpu':
                    continue
                child_info = BaseInfo(f"cpu{cpu}")
            else:
                child_info = BaseInfo(item[CHILD_TYPES[child_type]])
            child_info.id = item['_id']
            child_info.key = item['_key']
            child_info.type = item['type']

            if child_type == 'proc':
                app_ret = arango.get_out_edges(start=item['_id'],
                                               relation_type=str(ArangoRelation.BELONGS_TO),
                                               entity_type='app')
                if app_ret:
                    child_info.appId = app_ret[0]['_id']

            if child_type in ('proc', 'container'):
                child_info.containerId = ''
                if item.get('container_id'):
                    child_info.containerId = item['container_id']
                if child_type == 'container' or not child_info.containerId:
                    summary_info.edges.append({'from': item['_id'], 'to': entity_id})
            if child_type in ('container', 'app'):
                belong_entities = arango.get_in_edges(start=item['_id'],
                                                      relation_type=str(ArangoRelation.BELONGS_TO),
                                                      entity_type='proc')
                for proc_entity in belong_entities:
                    if child_type == 'container':
                        edge = {'from': proc_entity['_id'], 'to': item['_id']}
                    else:
                        edge = {'from': item['_id'], 'to': proc_entity['_id']}
                    summary_info.edges.append(edge)

            if abnormal_dict.get(item['_key']):
                summary_info.status = False
                child_info.status = False
                status = False

            data.append(child_info.__dict__)
        if child_type in ('cpu', 'nic', 'disk', 'mem'):
            summary_info.resource.extend(data)
        elif child_type in ('fs', 'net', 'sched'):
            summary_info.sys.extend(data)
        elif child_type in ('proc', 'container'):
            summary_info.proc.extend(data)
        elif child_type == 'app':
            summary_info.app.extend(data)

    return status, summary_info.__dict__


def get_entity_info(data: dict):
    exclude_list = ('level', 'timestamp', 'machine_id', '_id', '_key', '_rev', 'metrics')
    info = {}

    for key in data.keys():
        if key in exclude_list:
            continue
        info[key] = data[key]

    # 根据配置过滤出top指标
    metric_data = {}
    for key in data['metrics'].keys():
        value = custom_config.get_value(data['machine_id'], data['type'], key)
        if value is None:
            value = parse_bool_config(METRICS_CONF, f"Metrics-{data['type']}", key)
        if value:
            metric_data[key] = data['metrics'][key]

    return info, metric_data


def get_metric_range_data(metric_name: str, labels: dict, start: int, end: int, step: int):
    promtheus = connect_prometheus()

    res = promtheus.get_range_metrics(metric_name=metric_name, labels=labels,
                                      start_time=start, end_time=end, step=step)
    if res:
        return res[0]['values']
    else:
        return None


def get_normal_metric_range_data(metrics: dict, entity_type: str, labels: dict,
                                 start_time: int, end_time: int, step: int):
    ret = []
    for metric in metrics.keys():
        info = DataRate(metric_name=metric)
        range_data = get_metric_range_data(metric_name=f"{entity_type}_{metric}", labels=labels,
                                           start=start_time, end=end_time, step=step)
        if not range_data:
            return None
        for item in range_data:
            info.timeStamp.append(item[0] * 1000)
            info.values.append(item[1])
        ret.append(info.__dict__)

    return ret


def get_abnormal_metric_range_data(metric: dict, entity_type: str, labels: dict,
                                   start_time: int, end_time: int, step: int):
    res = AbnormalRate(metric_name=metric['metrics'], desc=metric['desc'],
                       event_id=metric['event_id'])
    ret = get_metric_range_data(metric_name=f"{entity_type}_{metric['metrics']}",
                                labels=labels, start=start_time, end=end_time, step=step)
    if not ret:
        return None
    for item in ret:
        res.timeStamp.append(item[0] * 1000)
        res.values.append(item[1])
    return res.__dict__


def get_children_topo(request: HttpRequest):
    """
    data struct:
    {
        status: success/failure
        data:
        {
            children:[{key:value},...],          与传入实体相关的下级观测实体的基本信息
            status: True/False
        }
    }
    """
    res = RespondResult()
    res.data = {'children': [], 'path': [], 'status': True}

    if request.method != 'POST':
        return HttpResponseNotAllowed("Wrong request method.")
    try:
        entity_id = str(request.POST['id'])
    except:
        return HttpResponseBadRequest("Invalid params.")

    try:
        in_edges_query = json.loads(request.POST['query'])
    except:
        in_edges_query = None

    try:
        arango = connect_arangodb(CONF.arangodb_db['spider'])
    except:
        return ConnectToArangoError()

    entity = arango.get_element_by_id(entity_id)
    abnormal_dict = get_abnormal_mapping(arango, entity_id)
    # 查询全部属于entity_id的实体信息，与entity_id同层
    relation = str(ArangoRelation.BELONGS_TO)
    query_keys = {'type': 'host', 'machine_id': entity['machine_id']}
    host_info = arango.get_elements_by_filter(entity['timestamp'], query_keys)[0]
    down_to_host = arango.get_shortest_path(entity_id, host_info['_id'])
    if in_edges_query:
        in_edges = arango.get_elements_by_filter(entity['timestamp'], in_edges_query)
    else:
        in_edges = arango.get_in_edges(start=entity_id, relation_type=relation)

    type_counts = {}
    for index in range(len(in_edges)):
        status = False if abnormal_dict.get(in_edges[index]['_key']) else True
        if not in_edges_query and status and type_counts.get(in_edges[index]['type'], 0) >= 10:
            continue
        sub_data = BaseInfo(combine_description_name(in_edges[index]))
        sub_data.id = in_edges[index]['_id']
        sub_data.key = in_edges[index]['_key']
        sub_data.type = in_edges[index]['type']
        sub_data.status = status
        if not sub_data.status:
            res.data['status'] = False
        else:
            type_counts[sub_data.type] = type_counts.get(sub_data.type, 0) + 1
        if in_edges[index]['_id'] not in down_to_host:
            res.data['children'].append(sub_data.__dict__)
    path_entities = arango.get_element_by_ids(down_to_host)
    if entity['type'] == 'proc':
        app = arango.get_out_edges(entity_id, relation, 'app')
        if app:
            path_entities.insert(0, app[0])
    for item in path_entities:
        path_info = BaseInfo(combine_description_name(item))
        path_info.id = item['_id']
        path_info.key = item['_key']
        path_info.type = item['type']
        path_info.status = res.data['status']
        res.data['path'].append(path_info.__dict__)
    res.status = str(RespondStatus.SUCCESS)
    return JsonResponse(res.__dict__)


def get_detail_by_id(request: HttpRequest):
    """
    data struct:
    {
        'status': 'success',
        'data':{
            'info':
            {
                key: value,
            } 观测实体基本信息key和label
            'metricInfo':
            {
                abnormal:                       异常的指标信息列表
                [
                    {
                        metricName:             异常的指标名
                        desc:                   指标名的中文描述
                        eventId:                对应的异常事件ID
                        timeStamp:[]            时间戳列表
                        values:[]               对应时间戳的值
                    },...
                ],
                normal:                         正常的指标信息
                [
                    {
                        metricName:             指标名
                        timeStamp:[]            时间戳列表
                        values:[]               对应时间戳的值
                    },...
                ],
            } 观测实体的指标信息
        },
    }
    """
    res = RespondResult()
    if request.method != "POST":
        return HttpResponseNotAllowed("Wrong request method.")

    try:
        entity_id = str(request.POST["id"])
    except:
        return HttpResponseBadRequest("Invalid params.")

    try:
        end = int(str(request.POST["end"])[:10])
    except:
        end = int(time.time())
    try:
        start = int(str(request.POST["start"])[:10])
    except:
        start = end - 3600

    try:
        interval = int(request.POST['interval'])
    except:
        interval = 30

    try:
        arango = connect_arangodb(CONF.arangodb_db['spider'])
    except:
        return ConnectToArangoError()

    ret = arango.get_element_by_id(eid=entity_id)
    if not ret:
        res.data = "Can't get data from ArangoDB."
        return JsonResponse(res.__dict__)

    entity_type = ret['type']
    entity_info = DetailBase()
    entity_info.info, entity_info.metricInfo['normal'] = get_entity_info(ret)
    # 筛选异常指标
    abnormal_list = arango.get_abnormal_metrics_by_id(entity_id)
    entity_info.metricInfo['abnormal'] = []
    exclude_keys = ('type', 'level', 'timestamp', 'metrics', '_id', '_key', '_rev')
    labels = {}

    for key in ret.keys():
        if key in exclude_keys:
            continue
        labels[key] = ret[key]
    if abnormal_list:
        for metric in abnormal_list:
            # 获取异常指标的一小时内的数据
            range_data = get_abnormal_metric_range_data(metric, entity_type, labels,
                                                        start, end, interval)
            if range_data:
                entity_info.metricInfo['abnormal'].append(range_data)
                if entity_info.metricInfo['normal'].get(metric['metrics']):
                    del entity_info.metricInfo['normal'][metric['metrics']]
    data = get_normal_metric_range_data(entity_info.metricInfo['normal'], entity_type, labels,
                                        start, end, interval)
    entity_info.metricInfo['normal'] = data

    res.data = entity_info.__dict__

    res.status = str(RespondStatus.SUCCESS)
    return JsonResponse(res.__dict__)


def get_entity_params_by_type(request: HttpRequest):
    """
    根据类型返回对应的指标名称列表
    输入：
        type：                                                   实体类型
    输出：
        data struct:
        {
            status: success/failure,                        请求响应的状态
            data:[]                                         指定实体类型的标签列表
        }
    """
    res = RespondResult()
    if request.method != "POST":
        return HttpResponseNotAllowed("Wrong request method.")
    try:
        entity_type = request.POST['type']
    except:
        return HttpResponseBadRequest("Invalid params.")

    res.data = []
    for params_type in ("keys", "labels"):
        data = get_observe_entity_by_type(entity_type).__dict__
        res.data.extend(data.get(params_type))
    if 'machine_id' in res.data:
        res.data.remove("machine_id")
    if 'hostname' in res.data:
        res.data.remove("hostname")
    res.status = str(RespondStatus.SUCCESS)
    return JsonResponse(res.__dict__)


def get_entities_value_by_label(request: HttpRequest):
    """
        根据类型及指标名称返回对应值的列表
        输入：
            machineId:                                              主机的machine_id
            type:                                                   待查询的实体类型
            label:                                                  待查询的标签名称
        输出：
            data struct:
            {
                status: success/failure,                        请求响应的状态
                data:[]                                         指定实体类型标签值的列表
            }
        """
    res = RespondResult()

    if request.method != "POST":
        return HttpResponseNotAllowed("Wrong request method.")

    try:
        entity_id = request.POST['id']
        machine_id = request.POST['machineId']
        entity_type = request.POST['type']
        label = request.POST['label']
    except:
        return HttpResponseBadRequest("Invalid params.")

    try:
        arango = connect_arangodb(CONF.arangodb_db['spider'])
    except:
        return ConnectToArangoError()

    entity = arango.get_element_by_id(entity_id)
    sub_name_mapping = {
        "container": "container_id",
        "proc": "tgid",
        "disk": "disk_name",
        "nic": "dev_name",
    }
    key = sub_name_mapping[entity['type']]
    query = {'machine_id': machine_id, 'type': entity_type, key: entity[key]}
    res.data = arango.get_values_by_label(filters=query, label=label)
    res.status = str(RespondStatus.SUCCESS)
    return JsonResponse(res.__dict__)
