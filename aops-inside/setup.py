from setuptools import setup
from setuptools import find_packages
from glob import glob


NAME = "aops-inside"
VERSION = "1.0.0"
DESC = "Aops-Inside O&M website service."
LONG_DESC = """
Aops-Inside is an O&M interface that helps users locate system problems 
through falme diagrams and topology diagrams.
"""

INSTALL_REQUIRES = [
    "django>=4.1.3",
    "pyarango>=2.0.1",
    "kafka-python>=2.0.2",
    "requests",
]

setup(
    name=NAME,
    version=VERSION,
    description=DESC,
    long_description=LONG_DESC,
    author_email="dutianyu1@huawei.com",
    url="https://gitee.com/sjh2022new/A-Ops.git",
    keywords=['Aops-Inside website'],
    python_requires=">=3.7",
    install_requires=INSTALL_REQUIRES,
    packages=find_packages(),
    include_package_data=True,
    data_files=[
        ("/etc/aops-inside/conf/", ["conf/aops-inside.conf", "conf/metrics.conf"]),
        ("/usr/lib/systemd/system/", ["aops-inside.service"]),
        ("/opt/aops-inside/frontend/", glob("frontend-out/*.*")),
        ("/opt/aops-inside/frontend/static/css/", glob("frontend-out/static/css/*")),
        ("/opt/aops-inside/frontend/static/fonts/", glob("frontend-out/static/fonts/*")),
        ("/opt/aops-inside/frontend/static/img/", glob("frontend-out/static/img/*")),
        ("/opt/aops-inside/frontend/static/js/", glob("frontend-out/static/js/*")),
    ],
)
