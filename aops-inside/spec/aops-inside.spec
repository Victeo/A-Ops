%define debug_package %{nil}

Name:            aops-inside
Version:         1.0.0
Release:         1
Summary:         A O&M website for A-Ops.
License:         MulanPSL2
URL:             https://gitee.com/openeuler/sjh2022new/A-Ops
Source:          %{name}-%{version}.tar.gz
BuildRoot:       %{_builddir}/%{name}-%{version}
BuildRequires:   procps-ng python3-setuptools
Requires:        python3-aops-inside = %{version}-%{release}

%description
O&M website module for A-Ops project

%package -n python3-aops-inside
Summary:         Python3 package of aops-inside
Requires:        python3-requests python3-pip

%description -n python3-aops-inside
Python3 package of aops-inside

%prep
%autosetup -n %{name}-%{version} -p1

%build
%py3_build

%install
%py3_install

%post

%preun

%postun
rm -rf /opt/%{name}

%files
%doc README.md
%license LICENSE
/opt/%{name}/frontend/*


%files -n python3-aops-inside
%config(noreplace) %{_sysconfdir}/%{name}/conf/aops-inside.conf
%config(noreplace) %{_sysconfdir}/%{name}/conf/metrics.conf
%{_unitdir}/aops-inside.service
%{python3_sitelib}/aops_inside/*
%{python3_sitelib}/aops_inside-*.egg-info

%post -n python3-aops-inside
%{_bindir}/pip3 install -r %{python3_sitelib}/aops_inside-*.egg-info/requires.txt >/dev/null 2>&1
%{_bindir}/find %{python3_sitelib}/aops_inside -type f -name '*.py' -exec chmod u+x {} \;
%{_bindir}/ln -sf %{python3_sitelib}/aops_inside/manage.py %{_bindir}/aops-inside
%{_bindir}/mkdir -p /opt/aops-inside
%{_bindir}/aops-inside makemigrations
%{_bindir}/aops-inside migrate

%preun -n python3-aops-inside
%{_bindir}/systemctl disable --now aops-inside.service

%postun -n python3-aops-inside
rm -f %{_bindir}/aops-inside
rm -rf /opt/%{name}/db.sqlite3

%changelog
* Fri Jan 13 2023 Tianyu Du <dutianyu1@huawei.com> - 1.0.0-1
- Package init
