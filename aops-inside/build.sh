#! /bin/bash

CUR_PATH=$(dirname "$(realpath "$0")")
RPMBUILD_PATH="${HOME}/rpmbuild"

function info()
{
    local msg=$*
    local date
    date=$(date +"%F %T")

    echo "[INFO] ${date} ${msg}"
}

function error()
{
    local msg=$*
    local date
    date=$(date +"%F %T")

    echo "[ERROR] ${date} ${msg}"
    exit 1
}

function main()
{
    info "Build foreground environment."
    npm -v || {
        yum install -y npm || error "Failed to find and install cmd[npm], please check it manually."
    }
    vue -V || {
        npm install @vue/cli -g >/dev/null 2>&1 || error "Failed to find and install cmd[vue], please check it manually."
    }
    cd "${CUR_PATH}/frontend" >/dev/null 2>&1 || error "Failed to enter dir ${CUR_PATH}/frontend."
    npm install || error "Failed to install frontend dependent packages."
    npm run build || error "Failed to build frontend."
    cd - >/dev/null 2>&1 || error "Failed to back to dir ${CUR_PATH}."
    info "Make aops-inside rpm package."
    [[ -d "${RPMBUILD_PATH}" ]] && rm -rf "${RPMBUILD_PATH}"
    mkdir -p "${RPMBUILD_PATH}"/{SOURCES,SPECS,SRPMS,RPMS,BUILD,BUILDROOT}
    python3 "${CUR_PATH}/setup.py" sdist || error "Failed to build backgroud source package."
    mv "${CUR_PATH}/dist"/aops-inside-*.tar.gz "${RPMBUILD_PATH}/SOURCES" || error "Not find src-package."
    rpmbuild --version > /dev/null 2>&1 || {
        yum install -y rpmrebuild || error "Failed to find and install cmd[rpmbuild], please check it manually."
    }
    rpmbuild -bb "${CUR_PATH}/spec/aops-inside.spec" || error "Failed to make aops-inside rpm packages."
}

info "Begin to build aops-inside."
main "${@}"
info "Succeeding building aops-inside."
exit 0
