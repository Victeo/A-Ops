const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  outputDir: '../frontend-out',
  assetsDir: 'static',

  devServer: {
    proxy: {
      '/api': {
        target: 'http://127.0.0.1:8000'
      }
    }
  }
})
