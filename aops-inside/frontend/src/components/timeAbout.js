const ONE_SECOND = 1000;
const ONE_MINUTE = 60 * ONE_SECOND;
const ONE_HOUR = 60 * ONE_MINUTE;
const ONE_DAY = ONE_HOUR * 24;

export const timeDict = {
    ONE_SECOND,
    ONE_MINUTE,
    ONE_HOUR,
    ONE_DAY
}

export const timeList = [
    {
        label: '自定义时间段',
        value: '',
    },
    {
        label: '近1小时',
        value: ONE_HOUR,
    },
    {
        label: '近6小时',
        value: ONE_HOUR * 6,
    },
    {
        label: '近12小时',
        value: ONE_HOUR * 12,
    },
    {
        label: '近1天',
        value: ONE_DAY,
    },
    {
        label: '近3天',
        value: ONE_DAY * 3,
    },
];

export const periodDict = {
    halfMin: {
        label: '30秒',
        value: ONE_SECOND * 30,
    },
    oneMin: {
        label: '1分钟',
        value: ONE_MINUTE,
    },
    fiveMin: {
        label: '5分钟',
        value: ONE_MINUTE * 5,
    },
    fifteenMin: {
        label: '15分钟',
        value: ONE_MINUTE * 15,
    },
    halfHour: {
        label: '30分钟',
        value: ONE_MINUTE * 30,
    },
    oneHour: {
        label: '1小时',
        value: ONE_HOUR,
    }
};

export function changeOption(timeLen) {
    if (timeLen <= ONE_HOUR) {
        return [
            periodDict.halfMin,
            periodDict.oneMin,
            periodDict.fiveMin
        ]
    }
    if (timeLen < ONE_HOUR * 6) {
        return [
            periodDict.oneMin,
            periodDict.fiveMin,
            periodDict.fifteenMin,
            periodDict.halfHour,
            periodDict.oneHour
        ]
    }
    if (timeLen < ONE_HOUR * 24) {
        return [
            periodDict.fiveMin,
            periodDict.fifteenMin,
            periodDict.halfHour,
            periodDict.oneHour
        ]
    }
    return [
        periodDict.halfHour,
        periodDict.oneHour
    ]

}
