export function fnSetChildCoordinates(nodeList, pX, thisY) {
    if (!nodeList.length) {
        return;
    }
    if (nodeList.length % 2) {
        // 奇数,处理中间节点
        const index = (nodeList.length - 1)/2;
        const middle = nodeList[index];
        middle.value = [pX, thisY];
        middle.y = thisY;
        middle.x = pX;
        fnSetLeft(nodeList, index, middle.x, thisY );
        fnSetRight(nodeList, index, middle.x, thisY);
    }else {
        // 偶数
        // 中间靠左的节点
        const indexLeft = nodeList.length / 2 - 1;
        const middleLeft = nodeList[indexLeft];
        middleLeft.value = [pX - 100, thisY];

        middleLeft.y = thisY;
        middleLeft.x = pX - 100;
        fnSetLeft(nodeList, indexLeft, middleLeft.x, thisY);
        // 中间靠右的节点
        const indexRight = nodeList.length / 2;
        const middleRight = nodeList[indexRight];
        middleRight.value = [pX + 100, thisY];

        middleRight.y = thisY;
        middleRight.x = pX + 100;
        fnSetRight(nodeList, indexRight, middleRight.x, thisY);
    }
}


function fnSetLeft(nodeList, index, x, y){
    if (index - 1 < 0) {
        return;
    }
    const thisNode = nodeList[index - 1];
    thisNode.value = [x - 200, y];
    thisNode.x = x - 200;
    thisNode.y = y;
    this.fnSetLeft(nodeList, index-1, x - 100, y)
}


function fnSetRight(nodeList, index, x, y){
    if (index + 1 === nodeList.length) {
        return;
    }
    const thisNode = nodeList[index + 1];
    thisNode.value = [x + 200, y];
    thisNode.x = x + 200;
    thisNode.y = y;
    thisNode.fixed = true;
    this.fnSetRight(nodeList, index + 1, x + 100, y)
}


export const verticalStra = {
    data() {
        const img = {
            app: require('@/assets/app_dark.png'),
            proc: require('@/assets/proc_dark.png'),
            sys: require('@/assets/sys_dark.png'),
            source: require('@/assets/source_dark.png'),
            host: require('@/assets/host_dark.png')
        }
        return {
            clusterList: {},
            level: [500, 750, 1050, 1450],
            levelDiv: [
                {
                    title: '应用层',
                    icon: img.app,
                    height: '115px',
                    class:'black'
                },
                {
                    title: '进程层',
                    icon: img.proc,
                    height: '175px',
                    class:'grey'
                },
                {
                    title: '系统层',
                    icon: img.sys,
                    height: '130px',
                    class:'black'
                },
                {
                    title: '资源层',
                    icon: img.source,
                    height: '130px',
                    class:'grey'
                },
                {
                    title: '主机层',
                    icon: img.host,
                    height: '265px',
                    class:'black'
                },
            ],
            color: {
                white: {
                    error: 'red',
                    app: 'rgb(176,255,192)',
                    proc: 'rgb(176,255,248)',
                    sys: 'rgb(195,233,255)',
                    source: 'rgb(176,208,255)',
                    host: 'rgb(147,147,255)',
                    node: '#37A2DA',
                    detailNode: '#98fffa',
                    backgroundColor: '#ffffff',
                    line: '#3f3f3f',
                },
                black: {
                    error: 'rgba(238,52,63,0.6)',
                    app: 'rgba(35,72,220,0.6)',
                    proc: 'rgba(1,163,182,0.6)',
                    sys: 'rgba(116,94,247,0.6)',
                    source: 'rgba(81, 104, 246, 0.6)',
                    host: 'rgba(138, 34, 188, 0.6)',
                    node: '#2448DC',
                    detailNode: '#01A3B6',
                    backgroundColor: '#272727',
                    line: '#ffffff'

                }
            },
            nowColor: {},
        }
    },
    mounted() {
        this.fnInitNowColor();
    },
    methods: {
        fnInitNowColor() {
            const theme = document.documentElement.getAttribute('theme') !== 'dark';
            if (theme){
                this.nowColor = this.color.white;
            }else {
                this.nowColor = this.color.black;
            }
        },
        fnSetCommonNodes(data, nodes, y) {
            data.forEach((item, index) => {
                const search = nodes.filter(val => val.id === item.id);
                if (!search.length) {
                    this.fnSetNode(item, nodes, (index + 1) * 150, y);
                }
            })
        },
        fnSetProc(procData, appData, nodes, y) {
            let index = 1;
            const containerList = [];
            const appList = [];
            const procDict = {};
            const appY = y + 220;
            const containerY = y - 190;
            procData.forEach(item => {
                if (item.type === 'container') {
                    const search = containerList.filter(val => val.id === item.id);
                    if (!search.length) {
                        containerList.push(item);
                    }
                    return;
                }
                if (!item.containerId){
                    // 没有容器的进程
                    this.fnSetNode(item, nodes, index * 150, y);
                    if (item.appId && !appList.includes(item.appId)) {
                        appList.push(item.appId);
                        const app = appData.filter(app => app.id === item.appId)[0];
                        this.fnSetNode(app, nodes, index * 150, appY);
                    }
                    index ++;
                    return;
                }
                if (procDict[item.containerId]) {
                    procDict[item.containerId].push(item);
                }else {
                    procDict[item.containerId] = [item];
                }
            });

            containerList.forEach(container => {
                const procList = procDict[container.containerId] || [];
                let x = ((procList.length - 1) / 2 + index) * 150;
                if (!procList.length){
                    x = index * 150;
                    index ++;
                }

                this.fnSetNode(container, nodes, x, containerY);
                procList.forEach(proc => {
                    this.fnSetNode(proc, nodes, index * 150, y);
                    if (proc.appId && !appList.includes(proc.appId)) {
                        appList.push(proc.appId);
                        const app = appData.filter(app => app.id === proc.appId)[0];
                        this.fnSetNode(app, nodes, index * 150, appY);
                    }
                    index ++;
                });

            });
            return index - 1;
        },
        fnSetSysAndResource(data, nodes, y) {
            data.forEach(item => {
                if (!this.clusterList[item.type]) {
                    this.clusterList[item.type] = [];
                    const self = this;
                    const node = {
                        name: item.type,
                        nodeType: 'collection',
                        type: '集合',
                        isCollapsed: true,
                        label: {
                            align:'center',
                            formatter() {
                                return [`{normal|${item.type}}`,` {number|${self.clusterList[item.type].length}}`].join('\n');
                            },
                            rich:{
                                normal: {
                                    padding: 16,
                                },
                                number: {
                                    backgroundColor: '#000',
                                    borderRadius:50,
                                    color: '#fff',
                                    padding: 3,
                                }
                            }
                        },
                        value: [Object.keys(this.clusterList).length * 150, y],
                    };
                    nodes.push(node);
                }
                this.clusterList[item.type].push(item);
            });
        },
        fnSetNode(item, nodes, x, y) {
            if(x !== undefined && y !== undefined) {
                item.value = [x, y];
            }
            if (!item.status) {
                item.itemStyle={
                    color: this.nowColor.error,
                }
            }
            nodes.push(item);
        },
        fnGetOptions(chartData, isFault) {
            return {
                xAxis: {
                    type: 'value',
                    show:false,
                },
                yAxis: {
                    type: 'value',
                    show:false,
                    max:1500,
                },
                grid: {
                    right: 100,
                    bottom: 160,
                    top: 50,
                    left: 50,
                    height:'90%'
                },
                visualMap: {
                    type: 'piecewise',
                    show: false,
                    dimension: 1,
                    seriesIndex: 0,
                    pieces: [
                        {
                            lt: this.level[0],
                            color: this.nowColor.host,
                        },
                        {
                            gt: this.level[0],
                            lt: this.level[1],
                            color: this.nowColor.source,
                        },
                        {
                            gt: this.level[1],
                            lt: this.level[2],
                            color: this.nowColor.sys,
                        },
                        {
                            gt: this.level[2],
                            lt: this.level[3],
                            color: this.nowColor.proc,
                        },
                        {
                            gt: this.level[3],
                            color: this.nowColor.app,
                        },
                    ]
                },
                dataZoom: {
                    type: 'inside'
                },
                tooltip: {
                    formatter(param) {
                        return param.data.type + ': ' + param.data.name;
                    },
                    backgroundColor:this.nowColor.backgroundColor,
                    textStyle:{
                        color: this.nowColor.line,
                    },

                },
                series: [
                    {
                        type: 'graph',
                        layout: 'none',
                        coordinateSystem: 'cartesian2d',
                        zoom: 1,
                        symbolSize: 60,
                        emphasis: {
                            focus: 'adjacency',
                            label: {
                                show: true
                            }
                        },
                        label: {
                            show: true,
                            width: 50,
                            overflow: 'breakAll',
                            formatter(param) {
                                let type = `{type|${param.data.type}:}\n`;
                                if (param.data.nodeType === 'sub') {
                                    type = '';
                                }

                                const nameMaxLen = 20 - param.data.type.length;
                                const name = param.data.name;
                                if (name.length <= nameMaxLen){
                                    return type + name;
                                }
                                return type + name.slice(0, nameMaxLen) + '...';
                            },
                            rich: {
                                type: {
                                    fontWeight: 'bolder',
                                }
                            }
                        },
                        edgeSymbol: ['circle', 'arrow'],
                        edgeSymbolSize: [4, 10],
                        edgeLabel: {
                            fontSize: 20
                        },

                        data: chartData.data,
                        // links: [],
                        links: chartData.links,
                        lineStyle: {
                            width: 2,
                            curveness: isFault? 0 : 0.2,
                        }
                    },
                ]
            };
        },
    }
}

