import {fnGetNextTopo} from "@/api/host";

export default {
    data() {
        return {
            // 详情拓扑数据
            detailTopo:{
                id: '',
                path: [],
                children: [],
            },
            typeList: [],
            selectStatus: '',
        }
    },
    methods:{
        async fnInitDetail(id) {
            this.detailTopo = {
                id,
                pathChart: [],
                children: [],
            }
            this.typeList = [];
            await this.fnViewNextTopo({id});
            this.detailTopo.children.forEach(item => {
                if (this.typeList.indexOf(item.type) === -1) {
                    this.typeList.push(item.type);
                }
            });
        },
        async fnViewNextTopo(param) {
            const { data } = await fnGetNextTopo(param);
            // param.id = "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_proc_441570";
            // const data = {
            //   status: "success",
            //   data: {
            //     children: [
            //       {
            //         name: "441570_208_POSTGRE_Q",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_sli_441570_208_POSTGRE_Q",
            //         status: true,
            //         type: "sli",
            //       },
            //       {
            //         name: "441570_210_POSTGRE_Q",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_sli_441570_210_POSTGRE_Q",
            //         status: true,
            //         type: "sli",
            //       },
            //       {
            //         name: "441570_218_POSTGRE_Q",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_sli_441570_218_POSTGRE_Q",
            //         status: true,
            //         type: "sli",
            //       },
            //       {
            //         name: "441570_222_POSTGRE_Q",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_sli_441570_222_POSTGRE_Q",
            //         status: true,
            //         type: "sli",
            //       },
            //       {
            //         name: "441570_135_POSTGRE_Q",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_sli_441570_135_POSTGRE_Q",
            //         status: true,
            //         type: "sli",
            //       },
            //       {
            //         name: "441570_139_POSTGRE_Q",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_sli_441570_139_POSTGRE_Q",
            //         status: true,
            //         type: "sli",
            //       },
            //       {
            //         name: "441570_137_POSTGRE_Q",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_sli_441570_137_POSTGRE_Q",
            //         status: true,
            //         type: "sli",
            //       },
            //       {
            //         name: "441570_141_POSTGRE_Q",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_sli_441570_141_POSTGRE_Q",
            //         status: true,
            //         type: "sli",
            //       },
            //       {
            //         name: "441570_205_POSTGRE_Q",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_sli_441570_205_POSTGRE_Q",
            //         status: true,
            //         type: "sli",
            //       },
            //       {
            //         name: "441570_207_POSTGRE_Q",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_sli_441570_207_POSTGRE_Q",
            //         status: true,
            //         type: "sli",
            //       },
            //       {
            //         name: "441570_15563_POSTGRE_0",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_sli_441570_15563_POSTGRE_0",
            //         status: false,
            //         type: "sli",
            //       },
            //       {
            //         name: "444850_441570_gaussdb",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_thread_444850",
            //         status: true,
            //         type: "thread",
            //       },
            //       {
            //         name: "444853_441570_gaussdb",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_thread_444853",
            //         status: true,
            //         type: "thread",
            //       },
            //       {
            //         name: "444854_441570_gaussdb",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_thread_444854",
            //         status: true,
            //         type: "thread",
            //       },
            //       {
            //         name: "444855_441570_gaussdb",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_thread_444855",
            //         status: true,
            //         type: "thread",
            //       },
            //       {
            //         name: "444856_441570_gaussdb",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_thread_444856",
            //         status: true,
            //         type: "thread",
            //       },
            //       {
            //         name: "444857_441570_gaussdb",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_thread_444857",
            //         status: true,
            //         type: "thread",
            //       },
            //       {
            //         name: "444858_441570_gaussdb",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_thread_444858",
            //         status: true,
            //         type: "thread",
            //       },
            //       {
            //         name: "445167_441570_gaussdb",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_thread_445167",
            //         status: true,
            //         type: "thread",
            //       },
            //       {
            //         name: "702481_441570_AVCworker",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_thread_702481",
            //         status: true,
            //         type: "thread",
            //       },
            //       {
            //         name: "4127566_441570_gaussdb",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_thread_4127566",
            //         status: true,
            //         type: "thread",
            //       },
            //       {
            //         name: "441570_0:0:0:0:0:0:.0.0.1",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_endpoint_441570_0:0:0:0:0:0:.0.0.1_0_connect",
            //         status: true,
            //         type: "endpoint",
            //       },
            //       {
            //         name: "10.244.193.226:36986_10.244.197.62:15401",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_tcp_link_441570_0_10.244.193.226_10.244.197.62_36986_15401_2",
            //         status: true,
            //         type: "tcp_link",
            //       },
            //       {
            //         name: "10.244.193.226:51737_10.244.197.62:15405",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_tcp_link_441570_0_10.244.193.226_10.244.197.62_51737_15405_2",
            //         status: true,
            //         type: "tcp_link",
            //       },
            //       {
            //         name: "10.244.197.203:58296_10.244.197.62:15400",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_tcp_link_441570_0_10.244.197.203_10.244.197.62_58296_15400_2",
            //         status: true,
            //         type: "tcp_link",
            //       },
            //       {
            //         name: "10.244.197.203:58298_10.244.197.62:15400",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_tcp_link_441570_0_10.244.197.203_10.244.197.62_58298_15400_2",
            //         status: true,
            //         type: "tcp_link",
            //       },
            //       {
            //         name: "10.244.197.203:58300_10.244.197.62:15400",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_tcp_link_441570_0_10.244.197.203_10.244.197.62_58300_15400_2",
            //         status: true,
            //         type: "tcp_link",
            //       },
            //       {
            //         name: "10.244.197.62:55600_10.244.197.62:15401",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_tcp_link_441570_0_10.244.197.62_10.244.197.62_55600_15401_2",
            //         status: true,
            //         type: "tcp_link",
            //       },
            //       {
            //         name: "10.244.197.62:57120_10.244.197.62:15400",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_tcp_link_441570_0_10.244.197.62_10.244.197.62_57120_15400_2",
            //         status: true,
            //         type: "tcp_link",
            //       },
            //       {
            //         name: "0:0:0:0:0:0:.0.0.1:43792_0:0:0:0:0:0:.0.0.1:15400",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_tcp_link_441570_0_0:0:0:0:0:0:.0.0.1_0:0:0:0:0:0:.0.0.1_43792_15400_10",
            //         status: true,
            //         type: "tcp_link",
            //       },
            //       {
            //         name: "0:0:0:0:0:0:.0.0.1:43792_0:0:0:0:0:0:.0.0.1:15400",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_tcp_link_441570_1_0:0:0:0:0:0:.0.0.1_0:0:0:0:0:0:.0.0.1_43792_15400_10",
            //         status: true,
            //         type: "tcp_link",
            //       },
            //       {
            //         name: "0:0:0:0:0:0:.0.0.1:43794_0:0:0:0:0:0:.0.0.1:15400",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_tcp_link_441570_0_0:0:0:0:0:0:.0.0.1_0:0:0:0:0:0:.0.0.1_43794_15400_10",
            //         status: true,
            //         type: "tcp_link",
            //       },
            //     ],
            //     path: [
            //       {
            //         name: "openGauss",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_app_10.244.197.62_15400",
            //         status: false,
            //         type: "app",
            //       },
            //       {
            //         name: "441570_gaussdb",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_proc_441570",
            //         status: false,
            //         type: "proc",
            //       },
            //       {
            //         name: "gauss1",
            //         id: "ObserveEntities_1665479861/ec28db3684174873b1b78b79700bc27c_host_openEuler-20.03-LTS-SP1",
            //         status: false,
            //         type: "host",
            //       },
            //     ],
            //     status: false,
            //   },
            // };
            this.detailTopo.children = data.data.children;
            this.fnFormatDetailNode(data.data, param.id);
        },
        fnFormatDetailNode(data, id) {
            // 路径
            this.detailTopo.pathChart = this.fnFormatDetailPath(data.path, id);
            const chart = this.fnFormatDetailChild(id);
            this.fnDrawDetailChart(chart);
        },
        fnFormatDetailPath(path, id) {
            const chart = {
                data: [],
                links: [],
            };
            for (let i = 0; i < path.length; i++) {
                if (i < path.length - 1) {
                    chart.links.push({
                        source: path[i].id,
                        target: path[i + 1].id,
                        lineStyle: {
                            width: 5,
                            curveness: 0.2,
                            opacity: 1,
                        },
                    });
                }

                let item = {
                    ...path[i],
                    symbolSize: 75,
                    itemStyle: {
                        color: this.nowColor.node,
                    },
                };
                if (i === 0) {
                    // 父节点
                    item.x = 100;
                    item.y = 100;
                    item.fixed = true;
                }
                if (path[i].id === id) {
                    // 选中当前节点
                    item.x = 500;
                    item.y = 300;
                    item.fixed = true;
                    item.symbolSize = 90;
                    item.nodeType = "leaf";
                }
                if (i === path.length - 1) {
                    // 主机
                    item = {
                        ...path[i],
                        nodeType: "host",
                        x: 1000,
                        y: 500,
                        symbolSize: 75,
                        itemStyle: {
                            color: this.nowColor.host,
                        },
                        fixed: true,
                    };
                }
                this.fnSetNode(item, chart.data);
            }
            return chart;
        },
        fnFormatDetailChild(id) {
            const chart = {
                data: [...this.detailTopo.pathChart.data],
                links: [...this.detailTopo.pathChart.links],
            };
            let children = this.detailTopo.children;
            if (this.selectStatus !== '') {
                children = this.detailTopo.children.filter(item => item.status === this.selectStatus);
            }
            children.forEach(item => {
                chart.links.push({
                    source: item.id,
                    target: id,
                });
                item.nodeType = "leaf";
                this.fnSetNode(item, chart.data);
            });
            return chart;
        },
        fnGetNodeDetailOptions(chartData) {
            return {
                tooltip: {
                    formatter(param) {
                        return param.data.type + ":" + param.data.name;
                    },
                    backgroundColor: this.nowColor.backgroundColor,
                    textStyle: {
                        color: this.nowColor.line,
                    },
                },
                series: [
                    {
                        type: "graph",
                        layout: "force",
                        symbolSize: 60,
                        emphasis: {
                            focus: "adjacency",
                            label: {
                                show: true,
                            },
                        },
                        zoom: 0.9,
                        itemStyle: {
                            color: this.nowColor.detailNode,
                        },
                        roam: true,
                        force: {
                            repulsion: 800,
                            edgeLength: [80, 110],
                        },
                        label: {
                            show: true,
                            width: 50,
                            overflow: "breakAll",
                            formatter(param) {
                                const type = `{type|${param.data.type}:}\n`;
                                const nameMaxLen = 10 - param.data.type.length;
                                const name = param.data.name;
                                if (name.length <= nameMaxLen) {
                                    return type + name;
                                }
                                return type + name.slice(0, nameMaxLen) + "...";
                            },
                            rich: {
                                type: {
                                    fontWeight: "bolder",
                                },
                            },
                        },
                        edgeSymbol: ["circle", "arrow"],
                        edgeSymbolSize: [4, 10],
                        edgeLabel: {
                            fontSize: 20,
                        },
                        data: chartData.data,
                        links: chartData.links,
                    },
                ],
            }
        },
        searchDetail(query) {
            const param = {
                id: this.detailTopo.id,
            };
            if (query) {
                param.query = JSON.stringify(query);
            }
            this.fnViewNextTopo(param);
        },
        changeStatus(status) {
            this.selectStatus = status;
            const chart = this.fnFormatDetailChild(this.detailTopo.id);
            this.fnDrawDetailChart(chart);
        }
    }
}
