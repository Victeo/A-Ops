import { Message } from 'element-ui';
//重复点击按钮或者同一个页面多个请求同时报错时,只展示一次
let messageInstance = null;
const resetMessage = (options) => {
  if(messageInstance) {
    messageInstance.close()
  }
  messageInstance = Message(options)
};
['error','success','info','warning'].forEach(type => {
  resetMessage[type] = options => {
    if(typeof options === 'string') {
      options = {
        message:options
      }
    }
    options.type = type
    return resetMessage(options)
  }
})
export const message = resetMessage