import instance from "../api/interceptor"

export function fnGetFaultList(param) {
    const params = new URLSearchParams(param);
    return instance.get('/fault_list', {params});
}

export function fnGetFaultTopo(params) {
    const param = new URLSearchParams(params);
    return instance.post('/fault_topo', param);
}