import instance from "../api/interceptor"

export function fnGetHostList(queryParam) {
    const params = new URLSearchParams(queryParam);
    return instance.get('/host_list', {params});
}

export function fnGetHostInfoById(params) {
    const param = new URLSearchParams(params);
    return instance.post('/host_info', param);
}

export function fnGetEntityDetail(params) {
    const param = new URLSearchParams(params);
    return instance.post('/entity_detail', param);
}

export function fnGetAllSubByPId(params) {
    const param = new URLSearchParams(params);
    return instance.post('/sub_detail', param);
}

export function fnGetMetric(params) {
    const param = new URLSearchParams(params);
    return instance.post('/range_data', param);
}

export function fnGetNextTopo(params) {
    const param = new URLSearchParams(params);
    return instance.post('/child_topo', param);
}

export function fnGetLabelsByType(params){
    const param = new URLSearchParams(params);
    return instance.post('/get_labels_by_type', param);
}

export function fnGetValuesByLabel(params){
    const param = new URLSearchParams(params);
    return instance.post('/get_values_by_label', param);
}
