import axios from 'axios'
import {message} from '../utils/resetMessage'

const instance=axios.create({})

// 请求拦截器
instance.interceptors.request.use(config=> {
   //==========  所有请求之前都要执行的操作  ==============
    return config;
  }, err=> {
   //==================  错误处理  ====================
    message({
      message: '请求超时!',
      type: 'error',
   })
    
    return Promise.resolve(err);
  })

//响应拦截器
   instance.interceptors.response.use(data=> {

   if (data.status && data.status == 200 && data.data.status == 'error') {
      message({
			message: data.data.msg,
			type: 'error',
		})

      return;
    }
    return data;
  }, err=> {
  //==============  错误处理  ====================
     if (err && err.response) {
          switch (err.response.status) {
              case 400: err.message = '请求错误(400)'; break;
              case 401: err.message = '未授权，请重新登录(401)'; break;
              case 403: err.message = '拒绝访问(403)'; break;
              case 404: err.message = '请求出错(404)'; break;
              case 408: err.message = '请求超时(408)'; break;
              case 500: err.message = '请求数据库失败(500)'; break;
              case 501: err.message = '服务未实现(501)'; break;
              case 502: err.message = '网络错误(502)'; break;
              case 503: err.message = '服务不可用(503)'; break;
              case 504: err.message = '网络超时(504)'; break;
              case 505: err.message = 'HTTP版本不受支持(505)'; break;
              case 600: err.message = 'Arango数据库连接错误，请检查配置'; break;
              case 601: err.message = 'Prometheus数据库连接错误，请检查配置'; break;
              default: err.message = `连接出错(${err.response.status})!`;
          }
      } else {
          err.message = '连接服务器失败!'
      }
     message({
			message: err.message,
			type: 'error',
		}) 
  
    return Promise.resolve(err);
  })
  export default instance