import instance from "../api/interceptor"

export function apiConfigureList(param) {
    const params = new URLSearchParams(param);
    return instance.get('/get_all_entity_types', { params });
}

export function apiGetGlobalConfig(param) {
    const params = new URLSearchParams(param);
    return instance.get('/get_global_config', { params });
}

export function apiSetGlobalConfig(params) {
    const param = new URLSearchParams(params);
    return instance.post('/set_global_config', param);
}

/* export function apiGetCustomConfig(params) {
    const param = new URLSearchParams(params);
    return instance.post('/get_custom_config', { param });
}

export function apiSetCustomConfig(params) {
    const param = new URLSearchParams(params);
    return instance.post('set_custom_config', param);
} */
export function apiGetCustomConfig(params) {
    const param = new URLSearchParams(params);
    return instance.post('/get_custom_config', param);
}

export function apiSetCustomConfig(params) {
    const param = new URLSearchParams(params);
    return instance.post('/set_custom_config', param);
}