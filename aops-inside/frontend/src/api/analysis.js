import instance from "../api/interceptor"

export function fnGetFlameList(params) {
    const param = new URLSearchParams(params);
    return instance.post('/get_flame_list', param);
}
