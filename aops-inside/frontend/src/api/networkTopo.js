
import instance from "../api/interceptor"

export function apiHorizontalInfo(params) {
    const param = new URLSearchParams(params);
    return instance.get('/horizontal_topo', {
        params: param
    })
}
