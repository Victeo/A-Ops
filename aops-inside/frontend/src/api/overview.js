import instance from "../api/interceptor"


export function apiOverviewInfo(params) {
    const param = new URLSearchParams(params);
    return instance.get('/overview', {
        params: param
    })
}
