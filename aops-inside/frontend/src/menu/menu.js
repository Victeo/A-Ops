
export default [
    {
        title: '概览',
        icon: require('./../assets/overview.png'),
        darkIcon: require('./../assets/overview_dark.png'),
        path: '/overview',
        component: () => import('@/views/overview/overview'),
    },
    {
        title: '实时拓扑',
        icon: require('./../assets/architecture.png'),
        darkIcon: require('./../assets/architecture_dark.png'),
        children: [
            {
                title: '集群拓扑',
                icon: require('./../assets/network_topo.png'),
                darkIcon: require('./../assets/network_topo_dark.png'),
                path: '/networkTopo',
                name: 'networkTopo',
                component: () => import('@/views/architecture/networkTopo'),
            },
            {
                title: '节点拓扑',
                icon: require('../assets/host_monitor_icon.png'),
                darkIcon: require('../assets/节点拓扑m.png'),
                path: '/hostMonitor',
                name: 'hostMonitor',
                component: () => import('@/views/monitor/hostMonitor'),
                children: [
                    {
                        title: '主机详情',
                        path: '/hostDetail',
                        name: 'hostDetail',
                        component: () => import('@/views/monitor/hostDetail'),
                    },
                ]
            },
        ]
    },
    {
        title: '故障诊断',
        icon: require('../assets/fault_diagnosis.png'),
        darkIcon: require('../assets/fault_diagnosis_dark.png'),
        path: '/faultDiagnosis',
        component: () => import('@/views/faultDiagnosis/faultDiagnosis'),
        children: [
            {
                title: '故障拓扑',
                path: '/faultTopo',
                name: 'faultTopo',
                component: () => import('@/views/faultDiagnosis/faultTopo'),
            }
        ]
    },
    {
        title: '性能瓶颈分析',
        icon: require('../assets/性能瓶颈选中.png'),
        darkIcon: require('../assets/性能瓶颈选中.png'),
        children: [
            {
                icon: require('./../assets/CPU默认.png'),
                darkIcon: require('./../assets/CPU默认.png'),
                title: 'cpu性能分析',
                path: '/cpuAnalysis',
                name: 'cpuAnalysis',
                component: () => import('@/views/analysis/cpuAnalysis'),
            },
            {
                icon: require('./../assets/内存选中.png'),
                darkIcon: require('./../assets/内存选中.png'),
                title: '内存性能诊断',
                path: '/memoryAnalysis.vue',
                name: 'memoryAnalysis',
                component: () => import('@/views/analysis/memoryAnalysis'),
            },
            {
                icon: require('./../assets/IO默认.png'),
                darkIcon: require('./../assets/IO默认.png'),
                title: '磁盘I/0性能分析',
                path: '/diskAnalysis',
                name: 'diskAnalysis',
                component: () => import('@/views/analysis/diskAnalysis'),
                children: [
                    {
                        icon: require('./../assets/IO默认.png'),
                        darkIcon: require('./../assets/IO默认.png'),
                        title: '磁盘I/0性能诊断/曲线图',
                        path: '/diskGraphs',
                        name: 'diskGraphs',
                        component: () => import('@/views/analysis/diskGraphs'),
                    }

                ]
            },

        ]
    },
    {
        title: '全局配置管理',
        icon: require('./../assets/全局配置管理m.png'),
        darkIcon: require('./../assets/全局配置管理m.png'),
        path: '/configureManage',
        component: () => import('@/views/configureManage/configureManage'),

    },
];
