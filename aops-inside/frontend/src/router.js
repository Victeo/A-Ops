import Vue from 'vue';
import VueRouter from 'vue-router';
import menu from "@/menu/menu";

const routes = [
    {
        path: "",
        redirect: '/index',
    },
    {
        path: "/index",
        redirect: '/overview',
    },
]

function setRoute(menuList) {
    menuList.forEach(model => {
        if (model.path) {
            let name = model.name;
            if (!name) {
                const pathArr = model.path.split('/');
                name = pathArr[pathArr.length - 1];
            }
            const thisRoute = {
                path: model.path,
                name: name,
            }
            thisRoute.component = model.component;
            routes.push(thisRoute);
        }
        if (model.children && model.children.length) {
            setRoute(model.children);
        }
    })

}

// 全局监听路由
/* routes.beforeEach(() => {
    sessionStorage.removeItem("isButtom");
});
 */


setRoute(menu);

Vue.use(VueRouter);
let router = new VueRouter({ routes: routes })
export default router
