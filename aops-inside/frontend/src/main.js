import Vue from 'vue'
import App from './App.vue'
import router from "./router"
import ElementUI from "element-ui"
import 'element-ui/lib/theme-chalk/index.css'
import './css/index.scss'
import * as echarts from 'echarts'

Vue.use(ElementUI, {
  size: 'small',
})
Vue.config.productionTip = false
Vue.prototype.$echarts = echarts;

new Vue({
  render: h => h(App), router
}).$mount('#aops')
