
# **1. pip源配置**
配置信任域，示例如下：
```shell
pip3 config set global.trusted-host 信任域地址
```
配置pip源的ulr地址，示例如下：
```shell
pip3 config set global.index-url 对应信任域的pip源地址
```

# **2. npm安装及配置**
## **npm工具安装**
**检查npm工具是否安装的方法：**
```shell
npm -v
```
返回npm工具版本号可直接进行npm源配置。

**以yum包管理方式说明如何安装npm工具：**

打开/etc/yum.repos.d/openEuler.repo配置文件，配置示例如下：
```shell
[openEuler-everything]
name=openEuler-everything
baseurl=可用yum源的url地址
enabled=1
gpgcheck=1
gpgkey=GPG key对应的url地址
```
完成配置后执行npm安装命令：
```shell
yum install -y npm
```
注：请确认所配置的yum源是否有npm工具的安装包。

## **npm源配置**
检查环境是否安装npm工具，未安装请先安装npm工具
npm源默认为国外的镜像源，国内需要进行换源，否则使用npm安装vue相关组件时会出现报错，配置示例如下：
```shell
npm config set registry 镜像源地址
```
# **3. 安装Aops-inside**
使用git下载aops-inside代码：
```shell
# ssh方式
git clone git@gitee.com:openeuler/A-Ops.git
```
```shell
# http方式
git clone https://gitee.com/openeuler/A-Ops.git
```
执行A-Ops/aops-inside/install.sh脚本进行安装:
# **4. 配置及运行**
使用vi命令打开/etc/aops-inside/aops-inside.conf文件，配置说明如下：

aops-inside服务的ip和port配置，用以启动aops-inside服务
```shell
[default]
backend_ip=访问aops-inside的ip
backend_port=端口号
```

ArangoDB:图数据库，用于获取构架感知拓扑数据。
```shell
[ArangoDB]
ip=访问ArangoDB的ip
port=端口号
db_name=数据库名称
```
Prometheus:时序数据库，用于获取指标数据
```shell
[Prometheus]
ip=访问Prometheus的ip
port=端口号
```
Kafka:消息中间件，用于获取异常事件和根因推导传播图
```shell
[Kafka]
ip=方位Kafka的ip
port=端口号
abnormal_topic=异常检测的topic
root_cause_topic=根因定位的topic
```

配置完成后执行命令启动服务：
```shell
systemctl enable --now aops-inside.service
```
